-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 18 mai 2020 à 03:18
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gs-emaquis`
--
CREATE DATABASE IF NOT EXISTS `gs-emaquis` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gs-emaquis`;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `categories`
--

TRUNCATE TABLE `categories`;
--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `code`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'ENT', 'Entrée', 1, '2020-05-16 12:58:51', '2020-05-16 12:58:51'),
(2, 'RES', 'Plat de résistance', 1, '2020-05-16 13:00:03', '2020-05-16 13:00:03'),
(3, 'DES', 'Desert', 1, '2020-05-16 13:00:34', '2020-05-16 13:00:34'),
(4, 'BOI', 'Boisson', 1, '2020-05-16 13:08:03', '2020-05-16 13:08:03');

-- --------------------------------------------------------

--
-- Structure de la table `delivery_mans`
--

DROP TABLE IF EXISTS `delivery_mans`;
CREATE TABLE IF NOT EXISTS `delivery_mans` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `delivery_mans`
--

TRUNCATE TABLE `delivery_mans`;
--
-- Déchargement des données de la table `delivery_mans`
--

INSERT INTO `delivery_mans` (`id`, `name`, `adress`, `phone`, `email`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Oumar Seydou', 'Abtta Eden Abidjan Riviera 3', '222222222', 'oumar@gmail.com', 1, '2020-05-17 22:07:19', '2020-05-17 22:07:19');

-- --------------------------------------------------------

--
-- Structure de la table `foods`
--

DROP TABLE IF EXISTS `foods`;
CREATE TABLE IF NOT EXISTS `foods` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `quantity_stock` int(11) NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foods_category_id_index` (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `foods`
--

TRUNCATE TABLE `foods`;
--
-- Déchargement des données de la table `foods`
--

INSERT INTO `foods` (`id`, `category_id`, `code`, `name`, `active`, `quantity_stock`, `unit_price`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 4, 'ORA', 'Orangina', 1, 200, 15, '', '', '2020-05-16 13:37:49', '2020-05-18 02:57:11'),
(2, 3, 'Ana', 'Ananas', 1, 30, 5, '', '', '2020-05-16 13:47:30', '2020-05-18 02:56:49'),
(3, 2, 'FOU', 'Foutou avec sauce arachide', 1, 60, 10, '', '', '2020-05-17 23:50:38', '2020-05-18 02:57:01'),
(4, 2, 'FOUGRAI', 'Foutou avec sauce sauce graine', 1, 0, 0, '', '', '2020-05-17 23:51:21', '2020-05-17 23:51:21');

-- --------------------------------------------------------

--
-- Structure de la table `maquis`
--

DROP TABLE IF EXISTS `maquis`;
CREATE TABLE IF NOT EXISTS `maquis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adress` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manager` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `maquis`
--

TRUNCATE TABLE `maquis`;
--
-- Déchargement des données de la table `maquis`
--

INSERT INTO `maquis` (`id`, `name`, `adress`, `phone`, `email`, `manager`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Alice', 'Marcory', '48232282', 'pierrerodolpheagnero@gmail.com', 'Tantie Alice', 1, '2020-05-17 21:41:13', '2020-05-17 21:41:13'),
(2, 'Restau Plus', 'Angré', '48232282', 'rodolpheagnero@yahoo.com', 'Rodolphe Agnero', 1, '2020-05-17 21:45:50', '2020-05-17 21:48:07');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_04_30_214247_create_profiles_table', 1),
(2, '2020_04_30_214129_create_transactions_table', 2),
(3, '2020_05_01_221016_add_column_order_to_transactions_table', 3),
(4, '2020_05_02_192735_create_wallets_table', 4),
(5, '2020_05_02_201032_add_more_columns_to_transactions_table', 5),
(6, '2020_05_04_110013_add_column_reference_to_transactions_table', 6),
(7, '2020_05_09_203034_add_column_active_to_users_table', 7),
(11, '2020_05_16_095657_create_orders_lines_table', 10),
(9, '2020_05_16_095636_create_orders_table', 9),
(12, '2020_05_18_023405_add_column_quatity_in_stock_to_foods_table', 11);

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` double NOT NULL DEFAULT '0',
  `delivery_adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number_foods` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `orders`
--

TRUNCATE TABLE `orders`;
-- --------------------------------------------------------

--
-- Structure de la table `orders_lines`
--

DROP TABLE IF EXISTS `orders_lines`;
CREATE TABLE IF NOT EXISTS `orders_lines` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `food_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_price` double NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_lines_quantity_unique` (`quantity`),
  UNIQUE KEY `orders_lines_total_price_unique` (`total_price`),
  KEY `orders_lines_order_id_index` (`order_id`),
  KEY `orders_lines_food_id_index` (`food_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `orders_lines`
--

TRUNCATE TABLE `orders_lines`;
-- --------------------------------------------------------

--
-- Structure de la table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `profiles`
--

TRUNCATE TABLE `profiles`;
--
-- Déchargement des données de la table `profiles`
--

INSERT INTO `profiles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Super administrateur', 'Utilisateurs avec toutes les attributions', NULL, NULL),
(2, 'Utilisateurs', 'Utilisateurs avec des accès limités', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `terms` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `users_profile_id_index` (`profile_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tronquer la table avant d'insérer `users`
--

TRUNCATE TABLE `users`;
--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `profile_id`, `name`, `username`, `phone`, `email`, `email_verified_at`, `password`, `terms`, `is_admin`, `remember_token`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, 'Rodolphe Agnero', 'ragnero', '48232282', 'pierrerodolpheagnero@gmail.com', NULL, '$2y$10$8f3qMLjO/e5x7Gt11.xT.Oh1lmOXyNB7mOHy2Ttih1OELKsyCmj7a', 1, 1, NULL, NULL, NULL, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
