<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Artisan::call('cache:clear');
Route::match(['get', 'post'],'/test/testftp','TestController@ftptest')->name('testftp');
Config::set('excel.csv.delimiter', ';');

// Authentication Routes
Route::match(['get', 'post'],'/register','Auth\RegisterController@register')->name('register');
Route::match(['get', 'post'], '/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout','Auth\LoginController@logout')->name('logout');
Route::match(['get', 'post'],'/email','Auth\ForgotPasswordController@email')->name('email');
Route::match(['get', 'post'],'/reset/{id}','Auth\ResetPasswordController@reset')->name('reset');
Route::match(['get', 'post'], '/change/{id}', 'Auth\LoginController@change')->name('change');
Route::match(['get', 'post'], '/profile/{id}', 'Auth\LoginController@profile')->name('profile');
Route::match(['get', 'post'], '/lock/{id}', 'Auth\LoginController@lock')->name('lock');

// Orders Routes
Route::get('/orders','OrderController@index')->name('orders');
Route::match(['get', 'post'],'/orders/create', 'OrderController@store')->name('orders.create');
Route::match(['get', 'post'],'/orders/categories/{category_id}/foods', 'OrderController@getFoodsByCategory')->name('orders.categories.{category_id}.foods');

// Profile Routes
Route::resource('profiles','ProfileController');
Route::post('profiles/{id}', 'ProfileController@delete');

// Users Routes
// Route::resource('users','UserController');
Route::match(['get', 'post'],'users','UserController@index')->name('users');
Route::post('users/{id}', 'UserController@delete');

// Categories Routes
Route::match(['get', 'post'],'categories','CategoryController@index')->name('categories');
Route::match(['get', 'post'],'categories/create','CategoryController@store')->name('categories.create');
Route::post('categories/{id}', 'CategoryController@delete');

// Foods Routes
Route::match(['get', 'post'],'foods','FoodController@index')->name('foods');
Route::match(['get', 'post'],'foods/create','FoodController@store')->name('foods.create');
Route::post('foods/{id}', 'FoodController@delete');

// Maquis Routes
Route::match(['get', 'post'],'maquis','MaquisController@index')->name('maquis');
Route::match(['get', 'post'],'maquis/create','MaquisController@store')->name('maquis.create');
Route::post('maquis/{id}', 'MaquisController@delete');

// DeliveriesMans Routes
Route::match(['get', 'post'],'deliveries_mans','DeliveryManController@index')->name('deliveries_mans');
Route::match(['get', 'post'],'deliveries_mans/create','DeliveryManController@store')->name('deliveries_mans.create');
Route::post('deliveries_mans/{id}', 'DeliveryManController@delete');


// Home routes
Route::get('/','HomeController@index')->name('home');;
Route::get('/home', 'HomeController@index')->name('home');;
Route::get('/dashboard', 'HomeController@index')->name('dashboard');;

