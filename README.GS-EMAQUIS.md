## LISEZ-MOI
=================================================================================================================

1. Compressez le dossier de projet entier sur votre ordinateur local. Vous obtiendrez un fichier zip - gs-emaquis.zip
2. Ouvrez votre cPanel d'hébergement.
3. Cliquez sur 'Gestionnaire de fichiers'
4. Cliquez sur 'Upload' ou Téléverser
5. Téléchargez le fichier emaquis.zip dans le répertoire racine - pas le public_html.
6. Extraire le emaquis.zip.
7. Ouvrez le dossier laravel50 et déplacez le contenu du dossier public dans le dossier public_html de votre cpanel. Vous pouvez également supprimer le dossier public vide maintenant.
8. Naviguez jusqu'au dossier public_html et localisez le fichier index.php. Faites un clic droit dessus et sélectionnez Code Edit dans le menu.
9. Cela ouvrira un autre onglet affichant l'éditeur de code cpanel.
10. changer les lignes suivantes (24 et 38) de
require __DIR__.'/../vendor/autoload.php';
$app = require_once __DIR__.'/../bootstrap/app.php';
à
require __DIR__.'/../emaquis/vendor/autoload.php';
$app = require_once __DIR__.'/../emaquis/bootstrap/app.php';

11. Ne pas modifier le contenu de votre fichier .htaccess
12. Si tout se passe bien, rendez-vous sur http://votredomaine.com , vous verrez le site s'afficher

NB: Avant tout, lancez composer install afin de mettre à jour les dépendances du projet
- Renommer le fichier env.exemple en .env et éditer ce fichier en mettant à jour les paramètres de la base de données

DB_DATABASE=gs-emaquis
DB_USERNAME=usrgsemaquis
DB_PASSWORD=gsemaquis@1605

- exécuter le script .sql que vous trouverez à la racine du projet dans votre MySQL


==================================================================================================================================
Il y'a  2 principaux profils:
1. Super administrateur: qui a tous les droits ( le droit de paramétrer des l'apps au travers du menu Configuration, de gérer les users (clients)
2. usagers ou clients: Peut Consulter les menus, passer des commandes et se faire livrer, il peut également suivre ses commandes, etc.

Un administrateur par défaut est créé avec les accès suivants:
pierrerodolpheagnero@gmail.com,ragnero,48232282 (vous pouvez changer ces accès une fois votre script .sql executé dans MySQL)
password: admin@3004

NB: Possibilité de se connecter avec son email, son numéro de téléphone, son nom d'utilisateur
==================================================================================================================================





