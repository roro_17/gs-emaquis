@extends('layouts.app')

@section('template_title')
    GS EMaquis - Categories
@endsection

@section('styles')
    <!-- Data Table CSS -->
    <link href="{{ URL::asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Configuration</a></li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i
                            data-feather="database"></i></span></span>Categories</h4>

                <div class="d-flex">
                    <a class="btn btn-sm btn-warning btn-wth-icon icon-wthot-bg mb-15 add" style="color: white !important;"><span
                            class="icon-label"><i class="fa fa-pencil"></i> </span><span class="btn-text">Créer une nouvelle catégorie </span></a>
                </div>

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Catégories</h5>
                    <p class="mb-40"> Visualiser la liste des <code>catégories</code>.</p>

                    @if (\Session::has('success'))
                        <div class="alert alert-inv alert-inv-success" role="alert">
                            {!! \Session::get('success') !!}
                        </div>
                    @endif

                    @if (\Session::has('error'))
                        <div class="alert alert-inv alert-inv-danger" role="alert">
                            {!! \Session::get('error') !!}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="datable_1" class="table table-hover w-100 display pb-30 myTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Libellé</th>
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $indexKey => $category)
                                        <tr>
                                            <td>{{ $category->id }}</td>
                                            <td>{{ $category->code}}</td>
                                            <td>{{ $category->name}}</td>
                                            <td>
                                                @if ($category->active === 1)  <span
                                                    class="badge badge-soft-warning">Actif</span>
                                                @elseif($order->active === 0) <span
                                                    class="badge badge-soft-danger">Désactivé</span>
                                                @endif

                                            </td>
                                            <td><a data-toggle="modal" data-id="{{$category->id}}"
                                                   data-code="{{$category->code}}"
                                                   data-name="{{$category->name}}"
                                                   data-active="{{$category->active}}"
                                                   class="pr-10 text-blue edit" data-toggle="tooltip" title=""
                                                   data-original-title="Voir et éditer"><i
                                                        class="zmdi zmdi-check"></i></a>
                                                <a onclick="deleteConfirmation({{$category->id}})" data-id="{{$category->id}}"
                                                   class="text-inverse" title="" data-toggle="tooltip"
                                                   data-original-title="Supprimer"><i
                                                        class="zmdi zmdi-delete"></i></a></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Libellé</th>
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalLarge01" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLarge01" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form class="needs-validation form-crud" novalidate role="form" method="post"
                                                  action="{{ route('categories.create') }}" name="form-crud"
                                                  id="form-crud">
                                                <div class="modal-body">

                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <input type="hidden" name="id" id="id" value="{{old('id')}}">
                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-10">
                                                            <label for="code">Code</label>
                                                            <input class="form-control"
                                                                   value="{{ old('code') }}"
                                                                   id="code" name="code"
                                                                   placeholder="Code" type="text">

                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-10">
                                                            <label for="name">Libellé</label>
                                                            <input class="form-control"
                                                                   value="{{ old('name') }}"
                                                                   required id="name" name="name"
                                                                   placeholder="Libellé" type="text">
                                                            @if ($errors->has('name'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('name'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Fermer
                                                    </button>
                                                    <button type="submit" id="submit" name="submit" class="btn btn-warning">Enregistrer &amp; Fermer</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </section>
            </div>


        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Data Table JavaScript -->
    <script src="{{ URL::asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/dataTables-data.js')}}"></script>


    <!-- BEGIN OTHER PAGE SCRIPT VALIDATION-->
    <script>
        /**
         * CHARGER MODAL FORM
         */
        $(document).on('click', '.add', function () { //dans l'ordre evenement/action , class de declenchement de l'action, function
            $('.modal-title').text('Créer'); // set title
            $('#submit-form').text('Créer'); // set label button
            $('.form-crud')[0].reset(); // reset form
            $('#exampleModalLarge01').modal('show'); // show modal
        });



        /* Submit Form Login */
        $('#submit').click(function (e) {
            debugger;
            var form = $("#form-crud");
            form.validate({
                rules: {
                    name:
                        {
                            required: true,
                            minlength: 3,
                        },
                },
                messages: {
                    name: {
                        required: 'Entrez le libellé',
                        minlength: 'Saisissez au moins 3 caractères'
                    },
                },
            });

        });
        /**
         * LOAD DATA FORM TO EDIT
         */
        $(document).on('click', '.edit', function () {
            $('.modal-title').text('Modifier'); // set title
            $('#submit-form').text('Modifier'); // set label button
            $('#form-crud')[0].reset(); // reset form
            // get data of user selected
            $('#id').val($(this).data('id'));
            $('#name').val($(this).data('name'));
            $('#active').val($(this).data('active'));
            $('#code').val($(this).data('code'));
            id = $('#id').val();
            $('#exampleModalLarge01').modal('show'); // show modal
        });
    </script>

    <script type="text/javascript">
        /**
         * DELETE DATA WITH CONFIRMATION
         */
        function deleteConfirmation(id) {
            swal({
                title: "Supprimer?",
                text: "Êtes-vous sûr de vouloir supprimer cet enregistrement?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui, supprimez-le!",
                cancelButtonText: "Non, annuler!",
                reverseButtons: !0
            }).then(function (e) {

                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: 'POST',
                        url: "{{url('/categories')}}/" + id,
                        data: {_token: CSRF_TOKEN},
                        dataType: 'JSON',
                        success: function (results) {

                            if (results.success === true) {
                                swal("Succès!", results.message, "success");
                            } else {
                                swal("Erreur!", results.message, "error");
                            }

                            document.location.href = 'users';
                        }
                    });

                } else {
                    e.dismiss;
                }

            }, function (dismiss) {
                return false;
            })
        }
    </script>
    <!-- END OTHER PAGE SCRIPT -->

@endsection
