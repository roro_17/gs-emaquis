@extends('layouts.app')

@section('template_title')
    GS EMaquis - Plats
@endsection

@section('styles')
    <!-- Data Table CSS -->
    <link href="{{ URL::asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Configuration</a></li>
            <li class="breadcrumb-item active" aria-current="page">Plats</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i
                            data-feather="database"></i></span></span>Plats</h4>

            <div class="d-flex">
                <a class="btn btn-sm btn-warning btn-wth-icon icon-wthot-bg mb-15 add" style="color: white !important;"><span
                        class="icon-label"><i class="fa fa-pencil"></i> </span><span class="btn-text">Créer un nouveau plat </span></a>
            </div>

        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Plats</h5>
                    <p class="mb-40"> Visualiser la liste des <code>plats</code>.</p>

                    @if (\Session::has('success'))
                        <div class="alert alert-inv alert-inv-success" role="alert">
                            {!! \Session::get('success') !!}
                        </div>
                    @endif

                    @if (\Session::has('error'))
                        <div class="alert alert-inv alert-inv-danger" role="alert">
                            {!! \Session::get('error') !!}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="datable_1" class="table table-hover w-100 display pb-30 myTable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Libellé</th>
                                        <th>Catégorie</th>
                                        <th>Prix unitaire</th>
                                        <th>Quantité en stock</th>
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($foods as $indexKey => $food)
                                        <tr>
                                            <td>{{ $food->id }}</td>
                                            <td>{{ $food->code}}</td>
                                            <td>{{ $food->name}}</td>
                                            <td>{{App\Food::getCategory($food->category_id)}}</td>
                                            <td>{{ $food->unit_price}}</td>
                                            <td>{{ $food->quantity_stock}}</td>
                                            <td>
                                                @if ($food->active === 1)  <span
                                                    class="badge badge-soft-warning">Actif</span>
                                                @elseif($order->active === 0) <span
                                                    class="badge badge-soft-danger">Désactivé</span>
                                                @endif

                                            </td>
                                            <td><a data-toggle="modal" data-id="{{$food->id}}"
                                                   data-code="{{$food->code}}"
                                                   data-name="{{$food->name}}"
                                                   data-active="{{$food->active}}"
                                                   data-category="{{$food->category_id}}"
                                                   data-quantity_stock="{{$food->quantity_stock}}"
                                                   data-unit_price="{{$food->unit_price}}"
                                                   class="pr-10 text-blue edit" data-toggle="tooltip" title=""
                                                   data-original-title="Voir et éditer"><i
                                                        class="zmdi zmdi-check"></i></a>
                                                <a onclick="deleteConfirmation({{$food->id}})" data-id="{{$food->id}}"
                                                   class="text-inverse" title="" data-toggle="tooltip"
                                                   data-original-title="Supprimer"><i
                                                        class="zmdi zmdi-delete"></i></a></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Libellé</th>
                                        <th>Catégorie</th>
                                        <th>Prix unitaire</th>
                                        <th>Quantité en stock</th>
                                        <th>Active</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalLarge01" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLarge01" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modifier</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form class="needs-validation form-crud" novalidate role="form" method="post"
                                                  action="{{ route('foods.create') }}" name="form-crud"
                                                  id="form-crud">
                                                <div class="modal-body">

                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    <input type="hidden" id="id" name="id" value="{{old('id')}}">
                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-10">
                                                            <label for="code">Code</label>
                                                            <input class="form-control"
                                                                   value="{{ old('code') }}"
                                                                   id="code" name="code"
                                                                   placeholder="Code" type="text">

                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-10">
                                                            <label for="name">Libellé</label>
                                                            <input class="form-control"
                                                                   value="{{ old('name') }}"
                                                                   required id="name" name="name"
                                                                   placeholder="Libellé" type="text">
                                                            @if ($errors->has('name'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('name'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-10">
                                                            <label for="name">Catégorie</label>
                                                            <select
                                                                class="form-control custom-select {{ $errors->has('category') ? 'has-error' : '' }}"
                                                                id="category" name="category" required>
                                                                <option value="">-- Sélectionner la catégorie --</option>
                                                                @foreach ($categories as $category)
                                                                    <option
                                                                        value="{{ $category->id }}" {{ (collect(old('category'))->contains($category->id)) ? 'selected':'' }}>{{ $category->name }}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('category'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('category') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('category'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('category') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-10">
                                                            <label for="name">Prix unitaire</label>
                                                            <input class="form-control"
                                                                   value="{{ old('unit_price') }}"
                                                                   required id="unit_price" name="unit_price"
                                                                   placeholder="Prix unitaire" type="number">
                                                            @if ($errors->has('unit_price'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('unit_price') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('unit_price'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('unit_price') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-6 mb-10">
                                                            <label for="name">Quantité disponible</label>
                                                            <input class="form-control"
                                                                   value="{{ old('quantity_stock') }}"
                                                                   required id="quantity_stock" name="quantity_stock"
                                                                   placeholder="Quantité disponible" type="number">

                                                        </div>
                                                    </div>


                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-10">
                                                            <label for="wallet">Téléverser ici votre image au <code>format .png, .jpeg, .jpg, .gif</code> afin de lancer l'opération</label>
                                                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">Télécharger</span>
                                                                </div>
                                                                <div class="form-control text-truncate" data-trigger="fileinput"><i
                                                                        class="glyphicon glyphicon-file fileinput-exists"></i> <span
                                                                        class="fileinput-filename"></span></div>
                                                                <span class="input-group-append">
														<span class=" btn btn-primary btn-file"><span
                                                                class="fileinput-new">Choisir le fichier</span><span
                                                                class="fileinput-exists">Modifier</span>
                                                <input type="file" id="image" name="image" value="{{ old('image') }}">
                                                </span>
                                                <a href="#" class="btn btn-secondary fileinput-exists"
                                                   data-dismiss="fileinput">Supprimer</a>
                                                </span>

                                                                @if ($errors->has('image'))
                                                                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                                                                @endif
                                                                @if (!$errors->has('image'))
                                                                    <span class="valid-feedback">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                                                                @endif

                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Fermer
                                                    </button>
                                                    <button type="submit" id="submit" name="submit" class="btn btn-warning">Enregistrer &amp; Fermer</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </section>
            </div>


        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Data Table JavaScript -->
    <script src="{{ URL::asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/dataTables-data.js')}}"></script>


    <!-- BEGIN OTHER PAGE SCRIPT VALIDATION-->
    <script>
        /**
         * CHARGER MODAL FORM
         */
        $(document).on('click', '.add', function () { //dans l'ordre evenement/action , class de declenchement de l'action, function
            $('.modal-title').text('Créer'); // set title
            $('#submit-form').text('Créer'); // set label button
            $('.form-crud')[0].reset(); // reset form
            $('#exampleModalLarge01').modal('show'); // show modal
        });



        /* Submit Form Login */
        $('#submit').click(function (e) {
            debugger;
            var form = $("#form-crud");
            form.validate({
                rules: {
                    name:
                        {
                            required: true,
                            minlength: 3,
                        },
                    category:
                        {
                            required: true,
                        },
                    unit_price:
                        {
                            required: true,
                        },
                    image: {
                        // required: true,
                        extension: "jpg|png|gif|jpeg" // |doc|docx|pdf|xls|rar|zip
                    },

                },
                messages: {
                    name: {
                        required: 'Le libellé est obligatoire',
                        minlength: 'Saisissez au moins 3 caractères'
                    },
                    category: {
                        required: 'Sélectionner la catégorie',
                    },
                    unit_price: {
                        required: 'Le prix unitaire est obligatoire',
                    },
                    image: {
                        // required: 'Vous devez charger une image',
                        extension: 'Veuillez sélectionner une image valide'
                    },
                },
            });

        });
        /**
         * LOAD DATA FORM TO EDIT
         */
        $(document).on('click', '.edit', function () {
            $('.modal-title').text('Modifier'); // set title
            $('#submit-form').text('Modifier'); // set label button
            $('#form-crud')[0].reset(); // reset form
            // get data of user selected
            $('#id').val($(this).data('id'));
            $('#name').val($(this).data('name'));
            $('#active').val($(this).data('active'));
            $('#code').val($(this).data('code'));
            $('#category').val($(this).data('category'));
            $('#quatity_stock').val($(this).data('quatity_stock'));
            $("#category > [value=" + $(this).data('category') + "]").attr("selected", "true");
            id = $('#id').val();
            $('#exampleModalLarge01').modal('show'); // show modal
        });
    </script>

    <script type="text/javascript">
        /**
         * DELETE DATA WITH CONFIRMATION
         */
        function deleteConfirmation(id) {
            swal({
                title: "Supprimer?",
                text: "Êtes-vous sûr de vouloir supprimer cet enregistrement?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui, supprimez-le!",
                cancelButtonText: "Non, annuler!",
                reverseButtons: !0
            }).then(function (e) {

                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: 'POST',
                        url: "{{url('/foods')}}/" + id,
                        data: {_token: CSRF_TOKEN},
                        dataType: 'JSON',
                        success: function (results) {

                            if (results.success === true) {
                                swal("Succès!", results.message, "success");
                            } else {
                                swal("Erreur!", results.message, "error");
                            }

                            document.location.href = 'users';
                        }
                    });

                } else {
                    e.dismiss;
                }

            }, function (dismiss) {
                return false;
            })
        }
    </script>
    <!-- END OTHER PAGE SCRIPT -->

@endsection
