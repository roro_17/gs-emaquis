@extends('layouts.app')

@section('template_title')
    GS EMaquis - Utilisateurs
@endsection

@section('styles')
    <!-- Data Table CSS -->
    <link href="{{ URL::asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Tableau de bord</a></li>
            <li class="breadcrumb-item active" aria-current="page">Utilisateurs</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i
                            data-feather="database"></i></span></span>Gérer les utilisateurs</h4>

           {{-- <div class="d-flex" style="display: none;">
                <button class="btn btn-sm btn-outline-light btn-wth-icon icon-wthot-bg mr-15 mb-15"><span class="icon-label"><i class="fa fa-user-plus"></i> </span><span class="btn-text">Créer un nouvel utilisateur </span></button>
                <button class="btn btn-sm btn-outline-light btn-wth-icon icon-wthot-bg mr-15 mb-15"><span class="icon-label"><i class="fa fa-print"></i> </span><span class="btn-text">Print </span></button>
                <button class="btn btn-sm btn-danger btn-wth-icon icon-wthot-bg mb-15"><span class="icon-label"><i class="fa fa-download"></i> </span><span class="btn-text">Report </span></button>
            </div>--}}
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Gestion des utilisateurs</h5>
                    <p class="mb-40"> Gérer les comptes des <code>utilisateurs</code>.</p>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="datable_1" class="table table-hover w-100 display pb-30 myTable">
                                    <thead>
                                    <tr>
                                        <th>Nom complet</th>
                                        <th>Nom d'utilisateur</th>
                                        <th>Téléphone</th>
                                        <th>Email</th>
                                        <th>Profile</th>
                                        <th>Créé le</th>
                                        <th>Statut</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $indexKey => $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->username }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{App\User::getProfile($user->profile_id)}}</td>
                                            <td>{{ date('d-m-Y H:i:s', strtotime($user->created_at)) }}</td>
                                            <td>
                                                @if ($user->active === 1)  <span
                                                    class="badge badge-soft-success">Actif</span>
                                                @else  <span class="badge badge-soft-danger ">Désactivé</span>
                                                @endif
                                            </td>

                                            <td><a data-toggle="modal" data-id="{{$user->id}}"
                                                   data-profile="{{$user->profile_id}}" data-terms="{{$user->terms}}"
                                                   data-name="{{$user->name}}"
                                                   data-username="{{$user->username}}"
                                                   data-password="{{$user->password}}"
                                                   data-phone="{{$user->phone}}" data-email="{{$user->email}}"
                                                   class="pr-10 text-blue edit" data-toggle="tooltip" title=""
                                                   data-original-title="Voir les détails sur l'utilisateur"><i
                                                        class="zmdi zmdi-check"></i></a>
                                                <a onclick="deleteConfirmation({{$user->id}})" data-id="{{$user->id}}"
                                                   class="text-inverse" title="" data-toggle="tooltip"
                                                   data-original-title="Supprimer l'utilisateur"><i
                                                        class="zmdi zmdi-delete"></i></a></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Nom complet</th>
                                        <th>Nom d'utilisateur</th>
                                        <th>Téléphone</th>
                                        <th>Email</th>
                                        <th>Profile</th>
                                        <th>Créé le</th>
                                        <th>Statut</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table> <!-- ./ table -->

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalLarge01" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLarge01" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Détails utilisateur</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form class="needs-validation form-user" novalidate role="form" method="post"
                                                  action="#" name="form-user"
                                                  id="form-user">
                                                <div class="modal-body">

                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    @if (\Session::has('success'))
                                                        <div class="alert alert-inv alert-inv-success" role="alert">
                                                            {!! \Session::get('success') !!}
                                                        </div>
                                                    @endif
                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-10">
                                                            <label for="name">Nom complet</label>
                                                            <input class="form-control"
                                                                   value="{{ ($user) ? $user->name : old('name') }}"
                                                                   required id="name" name="name"
                                                                   placeholder="Nom complet" type="text">
                                                            @if ($errors->has('name'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('name'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="username">Nom d'utilisateur</label>
                                                            <input class="form-control"
                                                                   value="{{ ($user) ? $user->username : old('username') }}"
                                                                   required id="username" name="username"
                                                                   placeholder="Nom d'utilisateur" type="text">
                                                            @if ($errors->has('username'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('username'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="email">Email</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"
                                                                          id="inputGroupPrepend">@</span>
                                                                </div>
                                                                <input class="form-control"
                                                                       value="{{ ($user) ? $user->email : old('email') }}"
                                                                       required id="email" name="email"
                                                                       placeholder="Adresse mail" type="email">
                                                                @if ($errors->has('email'))
                                                                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                                                @endif
                                                                @if (!$errors->has('email'))
                                                                    <span class="valid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-10">
                                                            <label for="phone">Numéro de téléphone</label>
                                                            <input class="form-control"
                                                                   value="{{ ($user) ? $user->phone : old('phone') }}"
                                                                   required id="phone" name="phone"
                                                                   placeholder="Téléphone" type="text">
                                                            @if ($errors->has('phone'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('phone'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="password">Mot de passe</label>
                                                            <input class="form-control" name="password" id="password"
                                                                   value="{{ old('password') }}"
                                                                   placeholder="Mot de passe" type="password">
                                                            @if ($errors->has('password'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('password'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="confirm_password">Confirmer mot de passe</label>
                                                            <input class="form-control" name="confirm_password"
                                                                   id="confirm_password"
                                                                   value="{{ old('confirm_password') }}"
                                                                   placeholder="Confirmer mot de passe" type="password">
                                                            @if ($errors->has('confirm_password'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('confirm_password'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;">
                                                        <div class="form-check custom-control custom-checkbox">
                                                            <input type="checkbox"
                                                                   class="form-check-input custom-control-input"
                                                                   id="invalidCheck" required>
                                                            <label class="form-check-label custom-control-label"
                                                                   for="invalidCheck">
                                                                Accepter les termes &amp; conditions
                                                            </label>
                                                            <div class="invalid-feedback">
                                                                Vous devez accepter avant de soumettre.
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Fermer
                                                    </button>
                                                    <button type="submit" class="btn btn-warning" style="display: none;">Sauver les changements</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </section>


            </div>

        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Data Table JavaScript -->
    <script src="{{ URL::asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/dataTables-data.js')}}"></script>


    <!-- BEGIN OTHER PAGE SCRIPT VALIDATION-->
    <script>
        /**
         * CHARGER MODAL FORM
         */
        $(document).on('click', '.add', function () { //dans l'ordre evenement/action , class de declenchement de l'action, function
            $('.modal-title').text('Créer un nouvel utilisateur'); // set title
            $('#submit-form').text('Créer un nouvel utilisateur'); // set label button
            $('.form-user')[0].reset(); // reset form
            $('#exampleModalLarge01').modal('show'); // show modal
        });

        /**
         * SUBMIT FORM
         */
        /* Hide / Show Password */
        /* $('#ckbpassword').change(function () {
             if ($(this).is(":checked")) {
                 $('#password').attr("type", "text");
                 $('#confirm_password').attr("type", "text");
                 $('#lblpassword').text("Cacher le mot de passe");
             } else {
                 $('#password').attr("type", "password");
                 $('#confirm_password').attr("type", "password");
                 $('#lblpassword').text("Afficher le mot de passe");
             }
         });*/

        /* Submit Form Login */
        $('#submit').click(function (e) {
            debugger;
            var form = $("#form-edit-profil");
            form.validate({
                rules: {
                    name:
                        {
                            required: true,
                            minlength: 3,
                        },
                    username:
                        {
                            required: true,
                            minlength: 4,
                        },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                    },
                    password: {
                        minlength: 6,
                        maxlength: 16
                    },
                    confirm_password: {
                        minlength: 6,
                        maxlength: 16,
                        equalTo: '#password'
                    },
                    terms: {
                        required: true
                    }
                },
                messages: {
                    name: {
                        required: 'Entrez votre nom complet',
                        minlength: 'Saisissez au moins 3 caractères'
                    },
                    username: {
                        required: 'Entrez votre nom d\'utilisateur ',
                        minlength: 'Saisissez au moins 3 caractères'
                    },
                    email: {
                        required: 'Entrez votre adresse email',
                        email: 'Entrez une adresse email valide'
                    },
                    phone: {
                        required: 'Entrez votre numéro de téléphone',
                    },
                    password: {
                        minlength: 'Minimum 6 caractères',
                        maxlength: 'Minimum 16 caractères'
                    },
                    confirm_password: {
                        minlength: 'Minimum 6 caractères',
                        maxlength: 'Minimum 16 caractères',
                        equalTo: 'Le mot de passe ne correspond pas'
                    },
                    terms: {
                        required: 'Vous devez accepter les termes & conditions'
                    }
                },
            });

        });
        /**
         * LOAD DATA FORM TO EDIT
         */
        $(document).on('click', '.edit', function () {
            $('.modal-title').text('Détails utilisateur'); // set title
            $('#submit-form').text('Modifier'); // set label button
            $('#form-user')[0].reset(); // reset form
            // get data of user selected
            $('#id').val($(this).data('id'));
            $('#name').val($(this).data('name'));
            $('#username').val($(this).data('username'));
            $('#email').val($(this).data('email'));
           /* $('#password').val($(this).data('password'));
            $('#password2').val($(this).data('password'));*/
            $('#profile').val($(this).data('profile'));
            $("#profile > [value=" + $(this).data('profile') + "]").attr("selected", "true");
            $('#terms').val($(this).data('terms'));
            $("#terms > [value=" + $(this).data('terms') + "]").attr("checked", "true");
            id = $('#id').val();
            $('#exampleModalLarge01').modal('show'); // show modal
        });
    </script>

    <script type="text/javascript">
        /**
         * DELETE DATA WITH CONFIRMATION
         */
        function deleteConfirmation(id) {
            swal({
                title: "Supprimer?",
                text: "Êtes-vous sûr de vouloir supprimer cet enregistrement?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Oui, supprimez-le!",
                cancelButtonText: "Non, annuler!",
                reverseButtons: !0
            }).then(function (e) {

                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax({
                        type: 'POST',
                        url: "{{url('/users')}}/" + id,
                        data: {_token: CSRF_TOKEN},
                        dataType: 'JSON',
                        success: function (results) {

                            if (results.success === true) {
                                swal("Succès!", results.message, "success");
                            } else {
                                swal("Erreur!", results.message, "error");
                            }

                            document.location.href = 'users';
                        }
                    });

                } else {
                    e.dismiss;
                }

            }, function (dismiss) {
                return false;
            })
        }
    </script>
    <!-- END OTHER PAGE SCRIPT -->
@endsection
