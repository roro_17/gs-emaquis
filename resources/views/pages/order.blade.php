@extends('layouts.app')

@section('template_title')
    GS EMaquis - Passer une commande
@endsection

@section('styles')

    <!-- Toastr CSS -->
    <link href="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet"
          type="text/css">

    <!-- ION CSS -->
    <link href="{{ URL::asset('vendors/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('vendors/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css')}}" rel="stylesheet"
          type="text/css">

    <!-- select2 CSS -->
    <link href="{{ URL::asset('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Pickr CSS -->
    <link href="{{ URL::asset('vendors/pickr-widget/dist/pickr.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Daterangepicker CSS -->
    <link href="{{ URL::asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Tableau de bord</a></li>
            <li class="breadcrumb-item active" aria-current="page">Commande</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">
        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i
                            data-feather="toggle-right"></i></span></span>Passer une commande</h4>
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">


                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Passer une commande</h5>
                    <p class="mb-40">Parcourez nos menus et passez vos commandes </p>
                    <div class="row">
                        <div class="col-xl-12">

                            <!-- Message -->
                            @if(Session::has('success'))
                                <div class="alert alert-success" role="alert">
                                    {{ Session::get('success') }}
                                </div>
                            @endif
                            @if(Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    {{ Session::get('error') }}
                                </div>
                        @endif

                        <!-- menus or foods -->

                            <section class="hk-sec-wrapper hk-gallery-wrap">
                                <ul class="nav nav-light nav-tabs ul-list-categories" role="tablist">

                                    @if(isset($categories) && count($categories) > 0)
                                        @foreach($categories as $indexKey => $category)
                                            <li class="nav-item">
                                                <a onclick="activatedLink('{{ $category->id }}')"
                                                   class="nav-link {{ (isset($category_id) && $category_id == $category['id']) ? 'active' : '' }}">
                                                    {{ $category->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    @endif


                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" role="tabpanel">
                                        <h6 class="mt-30 mb-20">Articles disponibles pour cette cat&eacute;gorie</h6>
                                        <div class="row hk-gallery">


                                            @if(isset($foods) && count($foods) > 0)


                                                <ul class="nav nav-pills nav-light pa-15 ul-list-foods" role="tablist">
                                                    @foreach($foods as $indexKey => $food)
                                                        <li class="nav-item"  style="margin-right: 30px;">
                                                            @if($food->image !== '' && $food->image !== null)

                                                                <div class="item">
                                                                    <div class="card">
                                                                        <img class="card-img-top" style="width: 260px; height: 159px;"
                                                                             src="../dist/img/foods/{{ $food->image }}.png"
                                                                             alt="Card image cap">
                                                                        <div class="card-body">
                                                                            <p class="card-text"> {{$food->name }} <span class="badge badge-soft-warning">{{'En stock : '.$food->quantity_stock}}</span></p>

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <a data-id="{{$food->id}}"
                                                                                       data-code="{{$food->code}}"
                                                                                       data-name="{{$food->name}}"
                                                                                       data-active="{{$food->active}}"
                                                                                       data-category_id="{{$food->category_id}}"
                                                                                       data-quantity_stock="{{$food->quantity_stock}}"
                                                                                       data-unit_price="{{$food->unit_price}}"
                                                                                       data-image="{{$food->image}}" data-toggle="tooltip" title="Plat"
                                                                                       style="min-width: 2.5em; color: white !important;"
                                                                                       class="btn btn-warning add-food-shopping-basket"
                                                                                       type="button">
                                                                                        <strong><i class="fa fa-shopping-basket"></i> Ajouter au panier</strong>
                                                                                    </a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @else

                                                                <div class="item">
                                                                    <div class="card">
                                                                        <img class="card-img-top" style="width: 260px; height: 159px;"
                                                                             src="../dist/img/foods/food-{{ $indexKey + 1 }}.png"
                                                                             alt="Card image cap">
                                                                        <div class="card-body">
                                                                            <p class="card-text"> {{$food->name}} <span class="badge badge-soft-warning">{{'En stock : '.$food->quantity_stock}}</span></p>

                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <a data-id="{{$food->id}}"
                                                                                       data-code="{{$food->code}}"
                                                                                       data-name="{{$food->name}}"
                                                                                       data-active="{{$food->active}}"
                                                                                       data-category_id="{{$food->category_id}}"
                                                                                       data-quantity_stock="{{$food->quantity_stock}}"
                                                                                       data-unit_price="{{$food->unit_price}}"
                                                                                       data-image="{{$food->image}}" data-toggle="tooltip" title="Plat"
                                                                                        style="min-width: 2.5em; color: white !important;"
                                                                                        class="btn btn-warning add-food-shopping-basket"
                                                                                        type="button">
                                                                                        <strong><i class="fa fa-shopping-basket"></i> Ajouter au panier</strong>
                                                                                    </a>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif


                                                        </li>
                                                    @endforeach

                                                </ul>



                                            @endif


                                        </div>
                                    </div>
                                </div>
                            </section>

                            <!-- ./ menus or foods -->


                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Toastr JS -->
    <script src="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
    {{--<script src="{{ URL::asset('dist/js/toast-data.js')}}"></script>--}}

    <!-- Jasny-bootstrap  JavaScript -->
    <script src="{{ URL::asset('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

    <!-- Ion JavaScript -->
    <script src="{{ URL::asset('vendors/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/rangeslider-data.js')}}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ URL::asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/select2-data.js')}}"></script>

    <!-- Bootstrap Tagsinput JavaScript -->
    <script src="{{ URL::asset('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>

    <!-- Bootstrap Input spinner JavaScript -->
    <script src="{{ URL::asset('vendors/bootstrap-input-spinner/src/bootstrap-input-spinner.js')}}"></script>
    <script src="{{ URL::asset('dist/js/inputspinner-data.js')}}"></script>

    <!-- Pickr JavaScript -->
    <script src="{{ URL::asset('vendors/pickr-widget/dist/pickr.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/pickr-data.js')}}"></script>

    <!-- Daterangepicker JavaScript -->
    <script src="{{ URL::asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ URL::asset('dist/js/daterangepicker-data.js')}}"></script>

    <script>
        $(document).ready(function () {


        });
    </script>

    <script>
        function activatedLink(id) {
            // call url to find all foods by category selected
            this.currentCategorie = id; // get current category
            // load foods or les articles in ajax
            debugger;
            var url = "{{url('orders/categories')}}/" + id + '/foods';
            $.ajax({
                url: url,
                type: 'GET',
            }).done(
                function (data) {
                    console.log('data', data);
                    debugger;
                    if (data != null) {
                        // load categorie
                        $('.ul-list-categories').empty();
                        for (i in data.categories) {
                            var li = '';
                            var category = data.categories[i];
                            var img = '';
                            var a = ''

                            if (id == category.id) {
                                a += "<a class='nav-link active' onclick=activatedLink(" + "'" + category.id + "'" + ");>";
                            } else {
                                a += "<a class='nav-link' onclick=activatedLink(" + "'" + category.id + "'" + ");>";
                            }

                            li += '<li class="nav-item">';
                            li += a + category.name +
                                '</a>' +
                                '</li>';

                            $('.ul-list-categories').append(li); // fill ul
                        }

                        // load foods by categorie
                        $('.ul-list-foods').empty();
                        for (i in data.foods) {
                            debugger;
                            var li = '';
                            var food = data.foods[i];
                            var img = '';

                            li += '<li class="nav-item" style="margin-right: 30px;">';
                            li += '<div class="item">';
                            li += '<div class="card">';

                            var idx= parseInt(i) + 1;

                            if (food.image == '' || food.image == null) {
                                img += '<img class="card-img-top" src="../dist/img/foods/food-' + idx + '.png" alt="Card image cap" style="width: 260px; height: 159px;">';
                            } else {
                                img += '<img class="card-img-top" src="../dist/img/foods/' + food.image + ' " alt="Card image cap" style="width: 260px; height: 159px;">';
                            }

                            var action= '<div class="row">'
                                +'<div class="col-md-12">'
                                +'<a data-id='+food.id + 'data-code='+food.code+ 'data-name='+food.name
                                +'data-active='+food.active+' data-category_id='+food.category_id +' data-quatity_stock='+food.quatity_stock
                                +'data-quantity-stock='+food.quantity_stock + ' data-unit_price='+food.unit_price
                                +' data-image='+food.image+ 'data-toggle="tooltip" title="Plat"'
                                +'style="min-width: 2.5em; color: white !important;" class="btn btn-warning add-food-shopping-basket" type="button">'
                                +'<strong><i class="fa fa-shopping-basket"></i> Ajouter au panier</strong>'
                                +'</a></div></div>'

                            li += img + '<div class="card-body">'
                                + '<p class="card-text">'+ food.name+' <span class="badge badge-soft-warning"> En stock : '+food.quantity_stock+'</span></p>'
                                + action
                                + '</div>'
                                + '</div>'
                                + '</div>';

                            $(".ul-list-foods").append(li);

                        }
                    }
                }
            );
        }

    </script>

@endsection
