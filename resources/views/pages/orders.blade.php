@extends('layouts.app')

@section('template_title')
    GS EMaquis - Liste des Commandes
@endsection

@section('styles')
    <!-- Data Table CSS -->
    <link href="{{ URL::asset('vendors/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ URL::asset('vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}"
          rel="stylesheet" type="text/css"/>
@endsection

@section('content')

    <!-- Breadcrumb -->
    <nav class="hk-breadcrumb" aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-light bg-transparent">
            <li class="breadcrumb-item"><a href="#">Tableau de bord</a></li>
            <li class="breadcrumb-item active" aria-current="page">Liste des Commandes</li>
        </ol>
    </nav>
    <!-- /Breadcrumb -->

    <!-- Container -->
    <div class="container">

        <!-- Title -->
        <div class="hk-pg-header">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"><i
                            data-feather="database"></i></span></span>Liste des Commandes</h4>

            @if($orders && count($orders) > 0)
                <div class="d-flex">
                    <a href="{{ route('export') }}" class="btn btn-sm btn-primary btn-wth-icon icon-wthot-bg mb-15"><span
                            class="icon-label"><i class="fa fa-download"></i> </span><span class="btn-text">Exporter la liste au format Excel </span></a>
                </div>
            @endif
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <h5 class="hk-sec-title">Liste des Commandes</h5>
                    <p class="mb-40"> Visulaiser la liste de toutes vos <code>commandes</code>.</p>
                    <div class="row">
                        <div class="col-sm">
                            <div class="table-wrap">
                                <table id="datable_1" class="table table-hover w-100 display pb-30 myTable">
                                    <thead>
                                    <tr>
                                        <th>R&eacute;f&eacute;rence</th>
                                        <th>Personne concernée</th>
                                        <th>Adresse de livraison</th>
                                        <th>Nombre de plats</th>
                                        <th>Montant (XOF)</th>
                                        <th>Date</th>
                                        <th>Statut</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $indexKey => $order)
                                        <tr>
                                            <td>{{ $order->reference }}</td>
                                            @if(session()->get('user') && session()->get('user')->is_admin == 1)
                                                <td>{{App\User::getNameUserById($order->user_id)}}</td>
                                            @endif
                                            <td>{{ number_format($order->number_foods, 0, ',', '.') }}</td>
                                            <td>{{ $order->delivery_adresse}}</td>
                                            <td>{{ number_format($order->total_price, 0, ',', '.') }}</td>
                                            <td>{{ date('d-m-Y H:i:s', strtotime($order->created_at)) }}</td>
                                            <td>
                                                @if ($order->status === 'in_preparation')  <span
                                                    class="badge badge-soft-warning">En cours de préparation</span>
                                                @elseif($order->status === 'cancelled') <span
                                                    class="badge badge-soft-danger">Annulée</span>
                                                @else  <span class="badge badge-soft-success ">Livrée</span>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>R&eacute;f&eacute;rence</th>
                                        <th>Personne concernée</th>
                                        <th>Adresse de livraison</th>
                                        <th>Nombre de plats</th>
                                        <th>Montant (XOF)</th>
                                        <th>Date</th>
                                        <th>Statut</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>


        </div>
        <!-- /Row -->

    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Data Table JavaScript -->
    <script src="{{ URL::asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/jszip/dist/jszip.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/dataTables-data.js')}}"></script>


    <script>
        $(document).ready(function () {
            // Internalization Data table
            /* $('.myTable').DataTable({
                   destroy: true,
                   language: {
                       url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/French.json'
                   }
               });*/
            /*$('#datable').DataTable({
                   destroy: true,
                   processing: "Traitement en cours...",
                   search: "Rechercher&nbsp;:",
                   lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                   info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                   infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                   infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                   infoPostFix: "",
                   loadingRecords: "Chargement en cours...",
                   zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                   emptyTable: "Aucune donnée disponible dans le tableau",
                   paginate: {
                       first: "Premier",
                       previous: "Pr&eacute;c&eacute;dent",
                       next: "Suivant",
                       last: "Dernier"
                   },
                   aria: {
                       sortAscending: ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                   }
           });*/
        });
    </script>

@endsection
