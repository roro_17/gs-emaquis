<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mairie de San-Pedro | Site Officiel | Mot de passe oublié</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="{{ URL::asset('assets/global/images/favicon.png')}}">
    <link href="{{ URL::asset('assets/global/css/style.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/global/css/ui.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/global/plugins/bootstrap-loading/lada.min.css')}}" rel="stylesheet">
</head>
<body class="account2" data-page="login">
<!-- BEGIN LOGIN BOX -->
<div class="container" id="login-block">
    <i class="user-img icons-faces-users-03"></i>
    <!-- BEGIN ACCOUNT INFO BOX -->
    <div class="account-info" style="display: none;">
        <a href="dashboard.html" class="logo"></a>
        <h3>Modular &amp; Flexible Admin.</h3>
        <ul>
            <li><i class="icon-magic-wand"></i> Fully customizable</li>
            <li><i class="icon-layers"></i> Various sibebars look</li>
            <li><i class="icon-arrow-right"></i> RTL direction support</li>
            <li><i class="icon-drop"></i> Colors options</li>
        </ul>
    </div>
    <!-- BEGIN ACCOUNT INFO BOX -->
    <div class="account-form">
        <!-- BEGIN FORM PASSWORD BOX -->
        <form class="form-password" role="form" method="POST" action="{{ route('email') }}" id="form-password" style="display: block">
            @csrf
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <h3><strong> Réinitialiser </strong> votre mot de passe </h3>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label class="control-label">Entrer votre adresse email</label>
                <div class="append-icon">
                    <input type="text" name="email" id="email" class="form-control form-white username" placeholder="Adresse email" value="{{ old('email') }}" required autofocus>
                    <i class="icon-user"></i>
                </div>
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <button type="submit" id="submit-password" class="btn btn-lg btn-warning btn-block ladda-button" data-style="expand-left" style="border-radius: 2px;">
                ENVOYER <i class="fa fa-send-o"></i></button>
            <div class="clearfix m-t-60">
                <p class="pull-left m-t-20 m-b-0"> Avez-vous un compte?<a id="login" href="{{ route('login') }}"> Se connecter</a></p>
                <p class="pull-right m-t-20 m-b-0" style="display:none;">Vous n'avez pas de compte?<a href="{{ route('register') }}"> S'inscrire</a></p>
            </div>
        </form>
        <!-- BEGIN FORM PASSWORD BOX -->
    </div>

    </div>
    <!-- BEGIN ACCOUNT BUILDER  BOX -->
    <div id="account-builder" style="display:none;">
        <form class="form-horizontal" role="form">
            <div class="row">
                <div class="col-md-12">
                    <i class="fa fa-spin fa-gear"></i>
                    <h3><strong>Customize</strong> Login Page</h3>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-xs-8 control-label">Social Login</label>
                        <div class="col-xs-4">
                            <label class="switch m-r-20">
                                <input id="social-cb" type="checkbox" class="switch-input" checked>
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-xs-8 control-label">Background Image</label>
                        <div class="col-xs-4">
                            <label class="switch m-r-20">
                                <input id="image-cb" type="checkbox" class="switch-input" checked>
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-xs-8 control-label">Background Slides</label>
                        <div class="col-xs-4">
                            <label class="switch m-r-20">
                                <input id="slide-cb" type="checkbox" class="switch-input">
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-xs-8 control-label">User Image</label>
                        <div class="col-xs-4">
                            <label class="switch m-r-20">
                                <input id="user-cb" type="checkbox" class="switch-input">
                                <span class="switch-label" data-on="On" data-off="Off"></span>
                                <span class="switch-handle"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- END ACCOUNT BUILDER BOX -->
</div>
<!-- END LOCKSCREEN BOX -->
<p class="account-copyright">
    <span>Copyright © 2015 </span><span>MAIRIE DE SAN-PEDRO</span>.<span>Tous droits réservés.</span>
</p>
<script src="{{ URL::asset('assets/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/gsap/main-gsap.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/tether/js/tether.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/icheck/icheck.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/backstretch/backstretch.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap-loading/lada.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/js/plugins.js')}}"></script>
<script src="{{ URL::asset('assets/global/js/pages/login-v2.js')}}"></script>
<script src="{{ URL::asset('assets/admin/layout1/js/layout.js')}}"></script>

<!-- SCRIPTS SWEET ALERT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.32.4/sweetalert2.all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
@include('sweet::alert')
</body>
</html>