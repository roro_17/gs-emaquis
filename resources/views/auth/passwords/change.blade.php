@extends('layouts.app')

@section('template_title')
    GS EMaquis - Changer mot de passe
@endsection

@section('styles')

    <!-- Toastr CSS -->
    <link href="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet"
          type="text/css">

    <!-- ION CSS -->
    <link href="{{ URL::asset('vendors/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('vendors/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css')}}" rel="stylesheet"
          type="text/css">

    <!-- select2 CSS -->
    <link href="{{ URL::asset('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Pickr CSS -->
    <link href="{{ URL::asset('vendors/pickr-widget/dist/pickr.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Daterangepicker CSS -->
    <link href="{{ URL::asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css"/>


    <style>
        .error {
            color: #f83f37 !important;
        }
    </style>
@endsection

@section('content')

    <!-- Container -->
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">

                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('dist/img/gallery/mock7.jpg')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            <img src="{{ URL::asset('dist/img/avatar12.jpg')}}" alt="user"
                                                 class="avatar-img rounded-circle">
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div
                                            class="text-white text-capitalize display-6 mb-5 font-weight-400">{{ session()->get('user') ? session()->get('user')->name : '' }}</div>
                                        <div class="font-14 text-white"><span class="mr-5"><span
                                                    class="font-weight-500 pr-5">Email : </span><span
                                                    class="mr-5">{{ session()->get('user') ? session()->get('user')->name : '' }}</span></span><span><span
                                                    class="font-weight-500 pr-5">Téléphone : </span><span>{{ session()->get('user') ? session()->get('user')->name : '' }}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="button-list">
                                    <a style="display: none;" href="#"
                                       class="btn btn-dark btn-wth-icon icon-wthot-bg btn-rounded"><span
                                            class="btn-text">Message</span><span class="icon-label"><i
                                                class="icon ion-md-mail"></i> </span></a>
                                    <a href="#" class="btn btn-icon btn-icon-circle btn-indigo btn-icon-style-2"><span
                                            class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                                    <a href="#" class="btn btn-icon btn-icon-circle btn-sky btn-icon-style-2"><span
                                            class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                                    <a href="#" class="btn btn-icon btn-icon-circle btn-purple btn-icon-style-2"><span
                                            class="btn-icon-wrap"><i class="fa fa-instagram"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white">
                    <div class="container">
                        <ul class="nav nav-light nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#" class="d-flex h-60p align-items-center nav-link active"><i
                                        class="fa fa-pencil"></i> Changer mon mot de passe</a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="tab-content mt-sm-60 mt-30">
                    <div class="tab-pane fade show active" role="tabpanel">
                        <div class="container">
                            <div class="hk-row">

                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Changer mon mot de passe</h5>
                                        <p class="mb-40">Ce formulaire vous permet de modifier le
                                            <code>mot de passe</code>de votre compte</p>
                                        <div class="row">
                                            <div class="col-sm">
                                                <form class="needs-validation" novalidate role="form" method="post"
                                                      action="{{ route('change',$id) }}" name="form-change-password"
                                                      id="form-change-password">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    @if (\Session::has('success'))
                                                        <div class="alert alert-inv alert-inv-success" role="alert">
                                                            {!! \Session::get('success') !!}
                                                        </div>
                                                    @endif
                                                    @if (\Session::has('error'))
                                                        <div class="alert alert-inv alert-inv-danger" role="alert">
                                                            {!! \Session::get('error') !!}
                                                        </div>
                                                    @endif
                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-10">
                                                            <label for="phone">Mot de passe actuel</label>
                                                            <input class="form-control"
                                                                   value="{{ old('oldpassword') }}"
                                                                   required id="oldpassword" name="oldpassword"
                                                                   placeholder="Mot de passe actuel" type="password">
                                                            @if ($errors->has('oldpassword'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('oldpassword') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('oldpassword'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('oldpassword') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="password">Nouveau mot de passe</label>
                                                            <input class="form-control" name="password" id="password"
                                                                   value="{{ old('password') }}" required
                                                                   placeholder="Mot de passe" type="password">
                                                            @if ($errors->has('password'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('password'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="confirm_password">Confirmer nouveau mot de passe</label>
                                                            <input class="form-control" name="password2"
                                                                   id="password2" required
                                                                   value="{{ old('password2') }}"
                                                                   placeholder="Confirmer nouveau mot de passe" type="password">
                                                            @if ($errors->has('confirm_password'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('password2') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('password2'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('password2') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;">
                                                        <div class="form-check custom-control custom-checkbox">
                                                            <input type="checkbox"
                                                                   class="form-check-input custom-control-input"
                                                                   id="invalidCheck" required>
                                                            <label class="form-check-label custom-control-label"
                                                                   for="invalidCheck">
                                                                Accepter les termes &amp; conditions
                                                            </label>
                                                            <div class="invalid-feedback">
                                                                Vous devez accepter avant de soumettre.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary" type="submit" id="submit"><i
                                                            class="fa fa-pencil"></i> Editer
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </section>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Toastr JS -->
    <script src="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
    {{--<script src="{{ URL::asset('dist/js/toast-data.js')}}"></script>--}}

    <!-- Jasny-bootstrap  JavaScript -->
    <script src="{{ URL::asset('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

    <!-- Ion JavaScript -->
    <script src="{{ URL::asset('vendors/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/rangeslider-data.js')}}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ URL::asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/select2-data.js')}}"></script>

    <!-- Bootstrap Tagsinput JavaScript -->
    <script src="{{ URL::asset('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>

    <!-- Bootstrap Input spinner JavaScript -->
    <script src="{{ URL::asset('vendors/bootstrap-input-spinner/src/bootstrap-input-spinner.js')}}"></script>
    <script src="{{ URL::asset('dist/js/inputspinner-data.js')}}"></script>

    <!-- Pickr JavaScript -->
    <script src="{{ URL::asset('vendors/pickr-widget/dist/pickr.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/pickr-data.js')}}"></script>

    <!-- Daterangepicker JavaScript -->
    <script src="{{ URL::asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ URL::asset('dist/js/daterangepicker-data.js')}}"></script>


    <script>
        $(document).ready(function () {
            /* Hide / Show Password */
            $('#ckbpassword').change(function () {
                if ($(this).is(":checked")) {
                    $('#password').attr("type", "text");
                    $('#confirm_password').attr("type", "text");
                    $('#lblpassword').text("Cacher le mot de passe");
                } else {
                    $('#password').attr("type", "password");
                    $('#confirm_password').attr("type", "password");
                    $('#lblpassword').text("Afficher le mot de passe");
                }
            });

            /* Submit Form Login */
            $('#submit').click(function (e) {
                debugger;
                var form = $("#form-change-password");
                form.validate({
                    rules: {
                        oldpassword: {
                            required: true
                        },
                        password: {
                            required: true,
                            minlength: 6,
                            maxlength: 16
                        },
                        password2: {
                            required: true,
                            minlength: 6,
                            maxlength: 16,
                            equalTo: '#password'
                        }
                    },
                    messages: {
                        oldpassword: {
                            required: 'Entrez votre mot de passe actuel',
                        },
                        password: {
                            required: 'Entrez votre nouveau mot de passe',
                            minlength: 'Minimum 6 caractères',
                            maxlength: 'Minimum 16 caractères'
                        },
                        password2: {
                            required: 'Confirmer votre nouveau mot de passe',
                            minlength: 'Minimum 6 caractères',
                            maxlength: 'Minimum 16 caractères',
                            equalTo: 'Le mot de passe ne correspond pas'
                        }
                    },
                });


            });

        });
    </script>

@endsection
