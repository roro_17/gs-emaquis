@extends('layouts.app')

@section('template_title')
    GS EMaquis - Profil
@endsection

@section('styles')

    <!-- Toastr CSS -->
    <link href="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet"
          type="text/css">

    <!-- ION CSS -->
    <link href="{{ URL::asset('vendors/ion-rangeslider/css/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('vendors/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css')}}" rel="stylesheet"
          type="text/css">

    <!-- select2 CSS -->
    <link href="{{ URL::asset('vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Pickr CSS -->
    <link href="{{ URL::asset('vendors/pickr-widget/dist/pickr.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Daterangepicker CSS -->
    <link href="{{ URL::asset('vendors/daterangepicker/daterangepicker.css')}}" rel="stylesheet" type="text/css"/>

    <style>
        .error {
            color: #f83f37 !important;
        }
    </style>
@endsection

@section('content')

    <!-- Container -->
    <div class="container-fluid">
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12 pa-0">

                <div class="profile-cover-wrap overlay-wrap">
                    <div class="profile-cover-img" style="background-image:url('dist/img/gallery/mock7.jpg')"></div>
                    <div class="bg-overlay bg-trans-dark-60"></div>
                    <div class="container profile-cover-content py-50">
                        <div class="hk-row">
                            <div class="col-lg-6">
                                <div class="media align-items-center">
                                    <div class="media-img-wrap  d-flex">
                                        <div class="avatar">
                                            <img src="{{ URL::asset('dist/img/avatar12.jpg')}}" alt="user"
                                                 class="avatar-img rounded-circle">
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div
                                            class="text-white text-capitalize display-6 mb-5 font-weight-400">{{ session()->get('user') ? session()->get('user')->name : '' }}</div>
                                        <div class="font-14 text-white"><span class="mr-5"><span
                                                    class="font-weight-500 pr-5">Email : </span><span
                                                    class="mr-5">{{ session()->get('user') ? session()->get('user')->name : '' }}</span></span><span><span
                                                    class="font-weight-500 pr-5">Téléphone : </span><span>{{ session()->get('user') ? session()->get('user')->name : '' }}</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="button-list">
                                    <a style="display: none;" href="#"
                                       class="btn btn-dark btn-wth-icon icon-wthot-bg btn-rounded"><span
                                            class="btn-text">Message</span><span class="icon-label"><i
                                                class="icon ion-md-mail"></i> </span></a>
                                    <a href="#" class="btn btn-icon btn-icon-circle btn-indigo btn-icon-style-2"><span
                                            class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                                    <a href="#" class="btn btn-icon btn-icon-circle btn-sky btn-icon-style-2"><span
                                            class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                                    <a href="#" class="btn btn-icon btn-icon-circle btn-purple btn-icon-style-2"><span
                                            class="btn-icon-wrap"><i class="fa fa-instagram"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bg-white">
                    <div class="container">
                        <ul class="nav nav-light nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#" class="d-flex h-60p align-items-center nav-link active"><i
                                        class="fa fa-pencil"></i> Editer mon profil</a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="tab-content mt-sm-60 mt-30">
                    <div class="tab-pane fade show active" role="tabpanel">
                        <div class="container">
                            <div class="hk-row">

                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Mettre &agrave; jour les informations de mon
                                            profil</h5>
                                        <p class="mb-40">Ce formulaire vous permet de modifier les
                                            <code>informations</code>de votre profil.</p>
                                        <div class="row">
                                            <div class="col-sm">
                                                <form class="needs-validation" novalidate role="form" method="post"
                                                      action="{{ route('profile',$id) }}" name="form-edit-profil"
                                                      id="form-edit-profil">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                    @if (\Session::has('success'))
                                                        <div class="alert alert-inv alert-inv-success" role="alert">
                                                            {!! \Session::get('success') !!}
                                                        </div>
                                                    @endif
                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-10">
                                                            <label for="name">Nom complet</label>
                                                            <input class="form-control"
                                                                   value="{{ ($user) ? $user->name : old('name') }}"
                                                                   required id="name" name="name"
                                                                   placeholder="Nom complet" type="text">
                                                            @if ($errors->has('name'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('name'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="username">Nom d'utilisateur</label>
                                                            <input class="form-control"
                                                                   value="{{ ($user) ? $user->username : old('username') }}"
                                                                   required id="username" name="username"
                                                                   placeholder="Nom d'utilisateur" type="text">
                                                            @if ($errors->has('username'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('username'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="email">Email</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"
                                                                          id="inputGroupPrepend">@</span>
                                                                </div>
                                                                <input class="form-control"
                                                                       value="{{ ($user) ? $user->email : old('email') }}"
                                                                       required id="email" name="email"
                                                                       placeholder="Adresse mail" type="email">
                                                                @if ($errors->has('email'))
                                                                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                                                @endif
                                                                @if (!$errors->has('email'))
                                                                    <span class="valid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-4 mb-10">
                                                            <label for="phone">Numéro de téléphone</label>
                                                            <input class="form-control"
                                                                   value="{{ ($user) ? $user->phone : old('phone') }}"
                                                                   required id="phone" name="phone"
                                                                   placeholder="Téléphone" type="text">
                                                            @if ($errors->has('phone'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('phone'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="password">Mot de passe</label>
                                                            <input class="form-control" name="password" id="password"
                                                                   value="{{ old('password') }}"
                                                                   placeholder="Mot de passe" type="password">
                                                            @if ($errors->has('password'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('password'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-4 mb-10">
                                                            <label for="confirm_password">Confirmer mot de passe</label>
                                                            <input class="form-control" name="confirm_password"
                                                                   id="confirm_password"
                                                                   value="{{ old('confirm_password') }}"
                                                                   placeholder="Confirmer mot de passe" type="password">
                                                            @if ($errors->has('confirm_password'))
                                                                <span class="invalid-feedback">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                                                            @endif
                                                            @if (!$errors->has('confirm_password'))
                                                                <span class="valid-feedback">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="display: none;">
                                                        <div class="form-check custom-control custom-checkbox">
                                                            <input type="checkbox"
                                                                   class="form-check-input custom-control-input"
                                                                   id="invalidCheck" required>
                                                            <label class="form-check-label custom-control-label"
                                                                   for="invalidCheck">
                                                                Accepter les termes &amp; conditions
                                                            </label>
                                                            <div class="invalid-feedback">
                                                                Vous devez accepter avant de soumettre.
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary" type="submit" id="submit"><i
                                                            class="fa fa-pencil"></i> Editer
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </section>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
    <!-- /Container -->

@endsection

@section('scripts')
    <!-- Toastr JS -->
    <script src="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
    {{--<script src="{{ URL::asset('dist/js/toast-data.js')}}"></script>--}}

    <!-- Jasny-bootstrap  JavaScript -->
    <script src="{{ URL::asset('vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

    <!-- Ion JavaScript -->
    <script src="{{ URL::asset('vendors/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/rangeslider-data.js')}}"></script>

    <!-- Select2 JavaScript -->
    <script src="{{ URL::asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/select2-data.js')}}"></script>

    <!-- Bootstrap Tagsinput JavaScript -->
    <script src="{{ URL::asset('vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>

    <!-- Bootstrap Input spinner JavaScript -->
    <script src="{{ URL::asset('vendors/bootstrap-input-spinner/src/bootstrap-input-spinner.js')}}"></script>
    <script src="{{ URL::asset('dist/js/inputspinner-data.js')}}"></script>

    <!-- Pickr JavaScript -->
    <script src="{{ URL::asset('vendors/pickr-widget/dist/pickr.min.js')}}"></script>
    <script src="{{ URL::asset('dist/js/pickr-data.js')}}"></script>

    <!-- Daterangepicker JavaScript -->
    <script src="{{ URL::asset('vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ URL::asset('dist/js/daterangepicker-data.js')}}"></script>


    <script>
        $(document).ready(function () {
            /* Hide / Show Password */
            /* $('#ckbpassword').change(function () {
                 if ($(this).is(":checked")) {
                     $('#password').attr("type", "text");
                     $('#confirm_password').attr("type", "text");
                     $('#lblpassword').text("Cacher le mot de passe");
                 } else {
                     $('#password').attr("type", "password");
                     $('#confirm_password').attr("type", "password");
                     $('#lblpassword').text("Afficher le mot de passe");
                 }
             });*/

            /* Submit Form Login */
            $('#submit').click(function (e) {
                debugger;
                var form = $("#form-edit-profil");
                form.validate({
                    rules: {
                        name:
                            {
                                required: true,
                                minlength: 3,
                            },
                        username:
                            {
                                required: true,
                                minlength: 4,
                            },
                        email: {
                            required: true,
                            email: true
                        },
                        phone: {
                            required: true,
                        },
                        password: {
                            minlength: 6,
                            maxlength: 16
                        },
                        confirm_password: {
                            minlength: 6,
                            maxlength: 16,
                            equalTo: '#password'
                        },
                        terms: {
                            required: true
                        }
                    },
                    messages: {
                        name: {
                            required: 'Entrez votre nom complet',
                            minlength: 'Saisissez au moins 3 caractères'
                        },
                        username: {
                            required: 'Entrez votre nom d\'utilisateur ',
                            minlength: 'Saisissez au moins 3 caractères'
                        },
                        email: {
                            required: 'Entrez votre adresse email',
                            email: 'Entrez une adresse email valide'
                        },
                        phone: {
                            required: 'Entrez votre numéro de téléphone',
                        },
                        password: {
                            minlength: 'Minimum 6 caractères',
                            maxlength: 'Minimum 16 caractères'
                        },
                        confirm_password: {
                            minlength: 'Minimum 6 caractères',
                            maxlength: 'Minimum 16 caractères',
                            equalTo: 'Le mot de passe ne correspond pas'
                        },
                        terms: {
                            required: 'Vous devez accepter les termes & conditions'
                        }
                    },
                });


            });

        });
    </script>

@endsection
