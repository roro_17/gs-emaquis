@extends('auth.layouts.app')

@section('template_title')
    GS EMaquis - Authentification
@endsection


@section('content')

    <div class="hk-pg-wrapper hk-auth-wrapper" style="background: rgb(241, 241, 245) !important;">
        <header class="d-flex justify-content-between align-items-center" style=" display: none !important;
">
            <a class="d-flex font-24 font-weight-500 auth-brand" href="#" style="color: orange !important; font-weight: 700;">
                GS EMaquis <!-- ou logo -->
            </a>
            <div class="btn-group btn-group-sm" style="display:none;">
                <a href="#" class="btn btn-outline-secondary">Help</a>
                <a href="#" class="btn btn-outline-secondary">About Us</a>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-7 pa-0">
                    <div class="auth-cover-img overlay-wrap" style="left: 0;
    padding: 156px 0px 56px 40px;
    text-align: left;
	display: none !important;"> <!-- style="background-image:url(dist/img/bg-2.jpg);" -->
                        <!--p>Grâce à MassPayment, toutes vos opérations deviennent possible </p-->
                    </div>
                </div>
                <div class="col-xl-5 pa-0">
                    <div class="auth-form-wrap py-xl-0 py-50" style="box-sizing: border-box;
    box-shadow: 0 2px 4px 0 rgba(181,181,181,.7);
    width: 360px;
    /* right: 10px; */
    min-height: 550px;
    z-index: 1;
    /* padding: 0 5px; */
    background: #fff;
    border-top: 1px solid #f1f1f5;">
                        <div class="auth-form w-xxl-55 w-sm-90 w-xs-100">
                            <form class="needs-validation" novalidate role="form" method="post"
                                  action="{{ route('login') }}" name="form-signin" id="form-signin">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <h1 class="display-5 mb-20" style="text-align: center;color: orange !important; font-weight: 700;"><strong>GS EMaquis</strong>
                                </h1> <!-- ou logo -->
                                <p class="mb-30" style="text-align: center;">Connectez-vous à votre compte et commander.</p>
                                <div class="form-group">
                                    <label for="username">Nom d'utilisateur, mail ou téléphone</label>
                                    <input class="form-control" id="username" name="username"
                                           value="{{ old('username') }}" required
                                           placeholder="Nom d'utilisateur, mail ou téléphone" type="text">
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('username'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Mot de passe</label>
                                    <input class="form-control" value="{{ old('password') }}" required
                                           placeholder="Mot de passe" id="password" name="password" type="password">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('password'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="custom-control custom-checkbox mb-25">
                                    <input class="custom-control-input" id="ckbpassword" name="ckbpassword"
                                           type="checkbox">
                                    <label class="custom-control-label font-14" id="lblpassword" name="lblpassword"
                                           for="ckbpassword">Afficher le mot de
                                        passe</label>
                                </div>
                                <button class="btn btn-primary btn-block" style="color: white !important;background: orange !important; border-color: orange !important;" type="submit" id="submit-form">Se connecter
                                </button>
                                <br> <br>
                                <p class="text-center" style="display: none;">Mot de passe oublié? <a href="#">Réinitialiser</a></p>
                                <p class="text-center">Pas de compte? <a href="{{ route('register') }}" style="color: orange !important;">S'inscrire</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            /* Hide / Show Password */
            $('#ckbpassword').change(function () {
                if ($(this).is(":checked")) {
                    $('#password').attr("type", "text");
                    $('#lblpassword').text("Cacher le mot de passe");
                } else {
                    $('#password').attr("type", "password");
                    $('#lblpassword').text("Afficher le mot de passe");
                }
            });

            /* Submit Form Login */
            $('#submit-form').click(function (e) {
                debugger;
                var form = $("#form-signin");
                form.validate({
                    rules: {
                        username: {
                            required: true,
                        },
                        password: {
                            required: true,
                        },
                    },
                    messages: {
                        username: {
                            required: 'Entrer le nom d\'utilisateur, email ou téléphone'
                        },
                        password: {
                            required: 'Entrer votre mot de passe',
                        },
                    },
                    submitHandler: function (form) {
                        return true;
                    }
                });

            });

        });
    </script>
@endsection
