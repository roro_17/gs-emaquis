<!DOCTYPE html>
<html>
<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>Mairie de San-Pedro - Site Officiel | Vérouiller</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="{{ URL::asset('assets/global/images/favicon.png')}}">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="{{ URL::asset('assets/global/css/style.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/global/css/ui.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/global/plugins/bootstrap-loading/lada.min.css')}}" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
</head>
<body class="account" data-page="lockscreen">
<!-- BEGIN LOGIN BOX -->
<div class="container" id="lockscreen-block">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="account-wall">
                <div class="user-image">
                    <img src="{{ URL::asset('assets/global/images/profil_page/friend8.jpg')}}" class="img-responsive img-circle" alt="friend 8">
                    <div id="loader"></div>
                </div>
                <form class="form-signin" role="form"  method="POST" action="{{ route('lock',$id) }}"> <!-- class="form-validation" -->
                    @csrf
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{ ($user) ? $user->id : old('id') }}">
                    <input type="hidden" name="email" value="{{ ($user) ? $user->email : old('email') }}">

                        @if (session()->get('user'))
                            <h2>Bienvenue, <strong>{{  session()->get('user')->firstname }} {{ session()->get('user')->lastname }}</strong>!</h2>
                        @endif

                            <p>Entrez votre mot de passe pour accéder au tableau de bord.</p>
                    <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Mot de passe">
                        <span class="input-group-btn"> 
                                <button type="submit" class="btn btn-warning" id="submit-lock">Déverouiller</button>
                                </span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="loader-overlay loaded">
    <div class="loader-inner">
        <div class="loader2"></div>
    </div>
</div>
<script src="{{ URL::asset('assets/global/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/jquery/jquery-migrate-3.0.0.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/gsap/main-gsap.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/tether/js/tether.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/backstretch/backstretch.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/bootstrap-loading/lada.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/plugins/progressbar/progressbar.min.js')}}"></script>
<script src="{{ URL::asset('assets/global/js/pages/lockscreen.js')}}"></script>
<script src="{{ URL::asset('assets/admin/layout1/js/layout.js')}}"></script>

<!-- SCRIPTS SWEET ALERT -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.32.4/sweetalert2.all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
@include('sweet::alert')

<script>
    var form = $("#form-signin");
    $('#submit-lock').click(function(e) {
        form.validate({
            success: "valid",
            errorClass: "form-error",
            validClass: "form-success",
            errorElement: "div",
            ignore: [],
            rules: {
                password: {
                    required: true,
                },
            },
            messages: {
                password: {
                    required: 'Entrer mot passe',
                },
            },
            highlight: function(element, errorClass, validClass) {
                $(element).closest('.form-control').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-control').removeClass(errorClass).addClass(validClass);
            },
            errorPlacement: function(error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                }
                else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                return true;
            }
        });

    });

    /**
     * reset form
     */
    $(".cancel").click(function() {
        form.resetForm();
    });
</script>
</body>
</html>
