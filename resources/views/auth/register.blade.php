@extends('auth.layouts.app')

@section('template_title')
    GS EMaquis - Inscription
@endsection

@section('styles')
    <style>
        .error {
            color: #f83f37 !important;
        }
    </style>
@endsection

@section('content')

    <div class="hk-pg-wrapper hk-auth-wrapper" style="background: rgb(241, 241, 245) !important;">
        <header class="d-flex justify-content-between align-items-center" style="    display: none !important;
">
            <a class="d-flex font-24 font-weight-500 auth-brand" href="#" style="color: orange !important; font-weight: 700;">
                GS EMaquis <!-- ou logo -->
            </a>
            <div class="btn-group btn-group-sm" style="display:none;" >
                <a href="#" class="btn btn-outline-secondary">Help</a>
                <a href="#" class="btn btn-outline-secondary">About Us</a>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-7 pa-0" >
                    <div class="auth-cover-img overlay-wrap" style="left: 0;
    padding: 156px 0px 56px 40px;
    text-align: left;
	display: none !important;"> <!-- style="background-image:url(dist/img/bg-2.jpg);" -->
                        <!--p>Grâce à GS EMaquis, toutes vos opérations deviennent possible </p-->
                    </div>
                </div>
                <div class="col-xl-5 pa-0">
                    <div class="auth-form-wrap py-xl-0 py-50" style="box-sizing: border-box;
    box-shadow: 0 2px 4px 0 rgba(181,181,181,.7);
    width: 360px;
    /* right: 10px; */
    min-height: 550px;
    z-index: 1;
    /* padding: 0 5px; */
    background: #ffffff;
    border-top: 1px solid #f1f1f5;">
                        <div class="auth-form w-xxl-55 w-sm-90 w-xs-100">
                            <form class="needs-validation" novalidate role="form" method="post"
                                  action="{{ route('register') }}" name="form-signup" id="form-signup">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <h1 class="display-5 mb-20" style="text-align: center;"><strong>GS EMaquis</strong></h1> <!-- ou logo -->
                                <p class="mb-30" style="text-align: center;">créer un compte et passer vos commandes.</p>

                                @if (\Session::has('success'))
                                    <div class="alert alert-inv alert-inv-success" role="alert">
                                            {!! \Session::get('success') !!}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <input class="form-control" value="{{ old('name') }}" required id="name" name="name" placeholder="Nom complet" type="text">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('name'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" value="{{ old('username') }}" required id="username" name="username" placeholder="Nom d'utilisateur" type="text">
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('username'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" value="{{ old('email') }}" required id="email" name="email" placeholder="Adresse mail" type="email">
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('email'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" value="{{ old('phone') }}" required id="phone" name="phone" placeholder="Téléphone" type="text">
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('phone'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="password" id="password" value="{{ old('password') }}" required placeholder="Mot de passe" type="password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('password'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="confirm_password" id="confirm_password" value="{{ old('confirm_password') }}" required placeholder="Confirmer mot de passe" type="password">
                                    @if ($errors->has('confirm_password'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('confirm_password'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                    </span>
                                    @endif
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" id="ckbpassword" name="ckbpassword" value="{{ old('ckbpassword') }}" type="checkbox">
                                    <label class="custom-control-label font-14" for="ckbpassword" id="lblpassword">Afficher le mot de passe</label>
                                    @if ($errors->has('ckbpassword'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('ckbpassword') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('ckbpassword'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('ckbpassword') }}</strong>
                    </span>
                                    @endif
                                </div>


                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" id="terms" name="terms" value="{{ old('terms') }}" type="checkbox" checked>
                                    <label class="custom-control-label font-14" for="terms">Accepter les termes et conditions</label>
                                    @if ($errors->has('terms'))
                                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('terms') }}</strong>
                    </span>
                                    @endif
                                    @if (!$errors->has('terms'))
                                        <span class="valid-feedback">
                        <strong>{{ $errors->first('terms') }}</strong>
                    </span>
                                    @endif
                                </div>



                                <button class="btn btn-primary btn-block" style="color: white !important;background: orange !important; border-color: orange !important;" type="submit" id="submit-form" name="submit-form">S'inscrire</button>
                                <br>
                                <p class="text-center">Déjà inscris? <a href="{{ route('login') }}" style="color: orange !important;">Se connecter</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            /* Hide / Show Password */
            $('#ckbpassword').change(function () {
                if ($(this).is(":checked")) {
                    $('#password').attr("type", "text");
                    $('#confirm_password').attr("type", "text");
                    $('#lblpassword').text("Cacher le mot de passe");
                } else {
                    $('#password').attr("type", "password");
                    $('#confirm_password').attr("type", "password");
                    $('#lblpassword').text("Afficher le mot de passe");
                }
            });

            /* Submit Form Login */
            $('#submit-form').click(function (e) {
                debugger;
                var form = $("#form-signup");
                form.validate({
                    rules: {
                        name:
                            {
                                required: true,
                                minlength: 3,
                            },
                        username:
                            {
                                required: true,
                                minlength: 4,
                            },
                        email: {
                            required: true,
                            email: true
                        },
                        phone: {
                            required: true,
                        },
                        password: {
                            required: true,
                            minlength: 6,
                            maxlength: 16
                        },
                        confirm_password: {
                            required: true,
                            minlength: 6,
                            maxlength: 16,
                            equalTo: '#password'
                        },
                        terms: {
                            required: true
                        }
                    },
                    messages: {
                        name: {
                            required: 'Entrez votre nom complet',
                            minlength: 'Saisissez au moins 3 caractères'
                        },
                        username: {
                            required: 'Entrez votre nom d\'utilisateur ',
                            minlength: 'Saisissez au moins 3 caractères'
                        },
                        email: {
                            required: 'Entrez votre adresse email',
                            email: 'Entrez une adresse email valide'
                        },
                        phone: {
                            required: 'Entrez votre numéro de téléphone',
                        },
                        password: {
                            required: 'Entrez votre mot de passe',
                            minlength: 'Minimum 6 caractères',
                            maxlength: 'Minimum 16 caractères'
                        },
                        confirm_password: {
                            required: 'Confirmer votre mot de passe',
                            minlength: 'Minimum 6 caractères',
                            maxlength: 'Minimum 16 caractères',
                            equalTo: 'Le mot de passe ne correspond pas'
                        },
                        terms: {
                            required: 'Vous devez accepter les termes & conditions'
                        }
                    },
                });


            });

        });
    </script>
@endsection
