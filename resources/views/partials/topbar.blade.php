<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar hk-navbar-alt">
    <a class="navbar-toggle-btn nav-link-hover navbar-toggler" href="javascript:void(0);" data-toggle="collapse"
       data-target="#navbarCollapseAlt" aria-controls="navbarCollapseAlt" aria-expanded="false"
       aria-label="Toggle navigation"><span class="feather-icon"><i data-feather="menu"></i></span></a>
    <a class="navbar-brand text-white" href="{{ route('home') }}" style="font-weight: 700;">
        GS EMaquis
    </a>
    <div class="collapse navbar-collapse" id="navbarCollapseAlt">
        <ul class="navbar-nav">

            <li class="nav-item show-on-hover {{ ($active_menu == 'home') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-dashboard"></i> Tableau de bord
                </a>
            </li>
            <li class="nav-item show-on-hover {{ ($active_menu == 'order') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('orders.create') }}" role="button" aria-expanded="false">
                    <i class="fa fa-shopping-basket"></i> Passer une commande
                </a>
            </li>

            @if(session()->get('user') && session()->get('user')->is_admin == 0)
                <li class="nav-item show-on-hover {{ ($active_menu == 'orders') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('orders') }}" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="fa fa-list"></i> Mes commandes
                    </a>
                </li>
            @endif

            @if(session()->get('user') && session()->get('user')->is_admin == 1)
                <li class="nav-item show-on-hover {{ ($active_menu == 'orders') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('orders') }}" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="fa fa-list"></i> Toutes les commandes
                    </a>
                </li>
                <li class="nav-item show-on-hover {{ ($active_menu == 'users') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('users') }}" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <i class="icon icon-people"></i> Utilisateurs
                    </a>
                </li>

                <li class="nav-item dropdown show-on-hover  {{ ($active_menu_parent == 'configuration') ? 'active' : '' }}">
                    <a class="nav-link dropdown-toggle" style="padding-right: 10px;" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Configuration
                    </a>
                    <div class="dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                        <a class="dropdown-item {{ ($active_menu == 'maquis') ? 'active' : '' }}" href="{{ route('maquis') }}">Maquis</a>
                        <a class="dropdown-item {{ ($active_menu == 'deliveries_mans') ? 'active' : '' }}" href="{{ route('deliveries_mans') }}">Livreur</a>
                        <a class="dropdown-item {{ ($active_menu == 'categories') ? 'active' : '' }}" href="{{ route('categories') }}">Catégorie de plat</a>
                        <a class="dropdown-item {{ ($active_menu == 'foods') ? 'active' : '' }}" href="{{{ route('foods') }}}">Plat</a>

                    </div>
                </li>

            @endif

        </ul>

    </div>
    <ul class="navbar-nav hk-navbar-content">
        <li class="nav-item dropdown dropdown-authentication">
            <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <div class="media">
                    <div class="media-img-wrap">
                        <div class="avatar">
                            <img src="{{ URL::asset('dist/img/avatar5.jpg')}}" alt="user"
                                 class="avatar-img rounded-circle">
                        </div>
                        <span class="badge badge-success badge-indicator"></span>
                    </div>
                    <div class="media-body">
                        <span>{{ session()->get('user') ? session()->get('user')->name : '' }}<i
                                class="zmdi zmdi-chevron-down"></i></span>
                    </div>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">

                @if(session()->get('user') && session()->get('user') != null)
                <a class="dropdown-item" href="{{ route('profile',session()->get('user')->id) }}"><i
                        class="dropdown-icon zmdi zmdi-account"></i><span>Profil</span></a> <!-- profile/{{session()->get('user')->id}} -->
                <a class="dropdown-item" href="{{ route('change',session()->get('user')->id) }}"><i
                        class="dropdown-icon zmdi zmdi-settings"></i><span>Changer mot de passe</span></a>
                @endif

                <div class="dropdown-divider"></div>
                <div class="sub-dropdown-menu show-on-hover">
                    <a href="#" class="dropdown-toggle dropdown-item no-caret"><i
                            class="zmdi zmdi-check text-success"></i>Connecté</a>
                    <div class="dropdown-menu open-left-side" style="display:none;">
                        <a class="dropdown-item" href="#"><i
                                class="dropdown-icon zmdi zmdi-check text-success"></i><span>Online</span></a>
                        <a class="dropdown-item" href="#"><i
                                class="dropdown-icon zmdi zmdi-circle-o text-warning"></i><span>Busy</span></a>
                        <a class="dropdown-item" href="#"><i
                                class="dropdown-icon zmdi zmdi-minus-circle-outline text-danger"></i><span>Offline</span></a>
                    </div>
                </div>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"><i
                        class="dropdown-icon zmdi zmdi-power"></i><span>Se déconnecter</span></a>
            </div>
        </li>
    </ul>
</nav>
