<div class="hk-footer-wrap " style="background: #ffffff;"> <!-- container -->
    <footer class="footer">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <p>Conçu par<a href="https://gs2e.com/" class="text-warning" target="_blank">GS2E</a> © 2020</p>
            </div>
            <div class="col-md-6 col-sm-12">
                <p class="d-inline-block">Suivez- nous</p>
                <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
            </div>
        </div>
    </footer>
</div>

