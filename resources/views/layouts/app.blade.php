<!DOCTYPE html>
<!--
Template Name: Scrooge - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Support: support@hencework.com

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">

<head>
    <meta charset="UTF-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') @endif</title>
    <meta name="author" content="NGSER">
    <meta name="description" content="Apps de paiement de masse en ligne"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('dist/img/favicon.ico')}}">
    <link rel="icon" href="{{ URL::asset('dist/img/favicon.ico')}}" type="image/x-icon">

    <!-- Toggles CSS -->
    <link href="{{ URL::asset('vendors/jquery-toggles/css/toggles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('vendors/jquery-toggles/css/themes/toggles-light.css')}}" rel="stylesheet"
          type="text/css">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('dist/css/style.css')}}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    @yield('styles')

<!-- Include this in your blade layout -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
<!-- HK Wrapper -->
<div class="hk-wrapper hk-alt-nav">

    <!-- Top Navbar -->
@include('partials.topbar')
<!-- /Top Navbar -->

    <!-- Main Content -->
    <div class="hk-pg-wrapper" style="background: rgb(241, 241, 245) !important;">
        <!-- Container -->
    @yield('content')
    <!-- /Container -->

        <!-- Footer -->
    @include('partials.footer')
    <!-- /Footer -->
    </div>
    <!-- /Main Content -->

</div>
<!-- /HK Wrapper -->

<!-- jQuery -->
<script src="{{ URL::asset('vendors/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('vendors/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{ URL::asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ URL::asset('dist/js/jquery.slimscroll.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ URL::asset('dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- FeatherIcons JavaScript -->
<script src="{{ URL::asset('dist/js/feather.min.js')}}"></script>

<!-- Toggles JavaScript -->
<script src="{{ URL::asset('vendors/jquery-toggles/toggles.min.js')}}"></script>
<script src="{{ URL::asset('dist/js/toggle-data.js')}}"></script>



<!-- Init JavaScript -->
<script src="{{ URL::asset('dist/js/init.js')}}"></script>


<!-- Sweetalert JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.32.4/sweetalert2.all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>

@include('sweet::alert')
@yield('scripts')

</body>

</html>
