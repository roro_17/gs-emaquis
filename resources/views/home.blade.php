@extends('layouts.app')

@section('template_title')
    GS EMaquis - Tableau de bord
@endsection

@section('styles')

    <!-- Toastr CSS -->
    <link href="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet"
          type="text/css">

    <!-- Morris Charts CSS -->
    <link href="{{ URL::asset('vendors/morris.js/morris.css')}}" rel="stylesheet" type="text/css"/>

@endsection

@section('content')

    <div class="container mt-xl-50 mt-sm-30 mt-15">
        <!-- Title -->
        <div class="hk-pg-header align-items-top">
            <div>
                <h2 class="hk-pg-title font-weight-600 mb-10">Tableau de bord</h2>
                <p style="display:none;">Vous donne une vue globale des commandes <a href="#"></a></p>
            </div>
        </div>
        <!-- /Title -->
        <!-- Row -->
        <div class="row">
            <div class="col-xl-12">
                <div class="hk-row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body pa-0">
                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <div id="support_table_wrapper" class="dataTables_wrapper">
                                            <table class="table display product-overview border-none dataTable  mb-0"
                                                   id="support_table" role="grid">
                                                <thead>
                                                <tr role="row">
                                                    @if(session()->get('user') && session()->get('user')->is_admin == 1)
                                                    <th class="sorting_asc" tabindex="0" aria-controls="support_table"
                                                        rowspan="1" colspan="1" style="width: 164px;"
                                                        aria-sort="ascending"
                                                        aria-label="ticket ID: activate to sort column descending">
                                                        Personne ayant commandé
                                                    </th>
                                                    @endif

                                                    <th class="sorting_asc" tabindex="0" aria-controls="support_table"
                                                        rowspan="1" colspan="1" style="width: 164px;"
                                                        aria-sort="ascending"
                                                        aria-label="ticket ID: activate to sort column descending">
                                                        Mode de payment
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="support_table"
                                                        rowspan="1" colspan="1" style="width: 203px;"
                                                        aria-label="Customer: activate to sort column ascending">
                                                        Nombre
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="support_table"
                                                        rowspan="1" colspan="1" style="width: 213px;"
                                                        aria-label="issue: activate to sort column ascending">Montant
                                                    </th>


                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($orders as $indexKey => $order)
                                                    <tr role="row">

                                                        @if(session()->get('user') && session()->get('user')->is_admin == 1)
                                                            <td>{{App\User::getNameUserById($order->user_id)}}</td>
                                                        @endif
                                                        <td>{{ $order->payment_mode }}</td>
                                                        <td>{{ $order->order_count }}</td>
                                                        <td>{{ number_format($order->order_mount, 0, ',', '.') }} XOF</td>

                                                         </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /Row -->
    </div>


@endsection

@section('scripts')
    <!-- Counter Animation JavaScript -->
    <script src="{{ URL::asset('vendors/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/jquery.counterup/jquery.counterup.min.js')}}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ URL::asset('vendors/raphael/raphael.min.js')}}"></script>
    <script src="{{ URL::asset('vendors/morris.js/morris.min.js')}}"></script>

    <!-- EChartJS JavaScript -->
    <script src="{{ URL::asset('vendors/echarts/dist/echarts-en.min.js')}}"></script>

    <!-- Owl JavaScript -->
    <script src="{{ URL::asset('vendors/owl.carousel/dist/owl.carousel.min.js')}}"></script>

    <!-- Toastr JS -->
    <script src="{{ URL::asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

   {{-- <script src="{{ URL::asset('dist/js/dashboard4-data.js')}}"></script>--}}

@endsection
