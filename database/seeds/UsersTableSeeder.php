<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Rodolphe Agnero',
            'username' => 'ragnero',
            'email' => 'pierrerodolpheagnero@gmail.com',
            'phone' => '48232282',
            'terms' => 1,
            'is_admin' => 1,
            'profile_id' => 1,
            'password' => Hash::make('admin@3004')
        ]);
    }
}
