<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('profiles')->insert([
            'name' => 'Super administrateur',
            'description' => 'Utilisateurs avec toutes les attributions',
        ]);

        DB::table('profiles')->insert([
            'name' => 'Utilisateurs',
            'description' => 'Utilisateurs avec des accès limités',
        ]);
    }
}
