<?php

namespace App\Http\Controllers;

use App\Maquis;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Auth;
use Session;
use Redirect;
use DB;

class MaquisController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    protected $dir = 'dist/img/maquis';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all maquiss
        $maquis = Maquis::orderBy('id', 'desc')->get();
        // $user = Auth::user(); // get user logged
        $user = $request->session()->get('user');
        // dd($user);
        /* if($user == null){
             return redirect('login');
         }*/
        $data = array(
            'title' => '',
            'active_menu' => 'maquis',
            'active_sub_menu' => 'maquis',
            'active_menu_parent' => 'configuration',
            'main_content' => 'maquis',
            'id' => $user->id,
            'user' => $user,
            'maquis' => $maquis,
            'add' => true,
        );
        // print_r($maquiss); exit;
        return view('pages.configuration.maquis', $data); //pass data to vue load
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r('OK'); exit;
        $id = $request->input('id');
        $maquis = Maquis::whereId($id)->first();
        if ($id) { // update maquis
            print_r('id defined');

            // set instance maquis
            $maquis->manager = $request->manager;
            $maquis->name = $request->name;
            $maquis->phone = $request->phone;
            $maquis->email = $request->email;
            $maquis->adress = $request->adress;
            $maquis->updated_at = date('Y-m-d H:i:s');
            // update maquis
            $maquis->save();
            print_r('maquis updated successful');

            if ($maquis) {
                Alert::success("Maquis modifié avec succès.")->persistent('Fermer');
                return redirect('maquis')->with('success', 'Maquis modifié avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('maquis')->with('error', 'Erreur survenue lors de la la mise à jour. Veuillez réessayer!');
            }

        } else { // create maquis
            // get next id
            $statement = DB::select("show table status like 'maquis'"); // requete pour recuperer le prochain id d'un autoincrement
            $id = $statement[0]->Auto_increment; // prochain id

            $maquis = Maquis::create([
                'manager' => $request->input('manager'),
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'adress' => $request->input('adress'),
                'active' => 1,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
            $maquis->save();

            if ($maquis) {
                Alert::success("Maquis créé avec succès.")->persistent('Fermer');
                return redirect('maquis')->with('success', 'Maquis créé avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('maquis')->with('error', 'Erreur survenue lors de l\'enregistrement. Veuillez réessayer!');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $delete = Maquis::where('id', $id)->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Maquis supprimé avec succès";
        } else {
            $success = true;
            $message = "Maquise non trouvé";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
