<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticateUsers;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Session;
use Redirect;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirm' => 'required|same:password',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function register(Request $request)
    {
        if ($request->isMethod('GET')) {
            $data = array(
                'title' => 'GS EMaquis - Inscription',
                'active_menu' => 'register',
                'main_content' => 'register',
            );
            return view('auth.register', $data); //pass data to vue load
        }

        if ($request->isMethod('POST')) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'string', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'password_confirm' => 'required|same:password',
                'terms' => 'accepted'
            ]);
            // initializ instance user
            $user = new User();
            // set instance user
            $user->name = $request->name;
            $user->username = $request->username;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->active = 1;
            $user->is_admin = 0;
            $user->profile_id = 2;

            $user->created_at = date('Y-m-d H:i:s');
            $user->updated_at = date('Y-m-d H:i:s');

            if($request->has('terms')){//Checkbox checked
                $user->terms = 1;
            } else{ //Checkbox not checked
                $user->terms = 0;
            }
            // display values form in console
            // dd($user);
            // check is email already used
            $usermail = User::whereEmail($user->email)->first();
            $userphone = User::wherePhone($user->phone)->first();
            $userusername = User::whereUsername($user->username)->first();
            if ($usermail) {
                Alert::error("Cette adresse email est déjà utilisée")->persistent('Fermer');
            } else if ($userphone) {
                Alert::error("Ce numéro de téléphone est déjà utilisé")->persistent('Fermer');
            } else if ($userusername) {
                Alert::error("Ce nom d'utilisateur est déjà utilisé")->persistent('Fermer');
            } else {
                // if ($request->password == $request->password_confirm) {
                    $user->save();
                    print_r('user created');
                    Alert::success('Félicitation !!! Votre compte a été créé avec succès. Profitez dès à présent de tous vos services en un clic.')->persistent('Fermer');
                    /** ecouteur d'exportation **/
                    // event(new UserRegistered());
                /*} else {
                    Alert::error('Mot de passe ne correspond pas')->persistent('Fermer');
                }*/
                return redirect()->back()->with('success', 'Félicitation !!! Votre compte a été créé avec succès. Profitez dès à présent de tous vos services en un clic.'); // rest on same page
            }
        }
        // return redirect()->back(); // rest on same page
    }
}
