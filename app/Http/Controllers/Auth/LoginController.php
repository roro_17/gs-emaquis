<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticateUsers;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Session;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    // use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectAfterLogout = '/login';

    public const HOME = '/dashboard';

    /**
     * Where to redirect users after logout.
     *
     * @var string
     */
    protected $redirectAfterLogin = '/lock';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function login(Request $request)
    {
        if ($request->isMethod('GET')) {
            $data = array(
                'title' => 'GS EMaquis - Authentification',
                'active_menu' => 'login',
                'main_content' => 'login',
            );
            return view('auth.login', $data); //pass data to vue load
        } else {
            // get user
            $user = User::where('username', $request->username)
                ->orWhere('email', $request->username)
                ->orWhere('phone', $request->username)
                ->first();
            // print_r($user); exit;
            if (!$user) {
                Alert::error("Nom d'utilisateur, email ou téléphone incorrect !")->persistent('Fermer');
                return redirect()->back();
            } else {
                if (Hash::check($request->password, $user->password)) {
                    $request->session()->put('user', $user); // put user in session
                    $profile = Profile::whereId($user->profile_id)->first();
                    $request->session()->put('profile', $profile); // put user in session
                    return redirect('/');
                } else {
                    Alert::error("Mot de passe incorrect")->persistent('Fermer');
                    return redirect()->back();
                }
            }
        }
    }

    /**
     * Logout, Clear Session, and Return.
     *
     * @return void
     */
    public function logout()
    {
        $user = Auth::user();
        Log::info('User Logged Out. ', [$user]);
        Auth::logout();
        Session::flush();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/login');
    }

    /**
     * Change password
     *
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function change(Request $request, $id = null)
    {
        // $user=User::whereId($id)->first();
        // $user = Auth::user();
        $user = $request->session()->get('user');
        // dd($user);
        if ($user == null) {
            return redirect('login');
        }
        if ($request->isMethod('post')) {

           /* $validator = Validator::make($request->all(), [
                'oldpassword' => ['required','string'],
                'password' => ['required','string', 'min:6', 'confirmed'],
                'password2' => ['required','same:password'],
            ]);
            if ($validator->fails()){
                return redirect()->back()->with('error', $validator->errors());
            }*/

            $email = $user->email;
            $oldpassword = $request->input('oldpassword');
            $password = $request->input('password');
            $password_confirm = $request->input('password2');
            // print_r(Hash::make($oldpassword));exit;
            // print_r($user->password);exit;
            if (!empty($oldpassword) AND !password_verify($oldpassword, $user->password)) {
                Alert::error('Le mot de passe n\'est pas celui que vous avez fourni lors de votre inscription. Veuillez réssayer SVP!')->persistent('Fermer'); // if well redirect page home
                return redirect()->back()->with('error', 'Le mot de passe n\'est pas celui que vous avez fourni lors de votre inscription. Veuillez réssayer SVP!');
            }
            if (!empty($password) AND !empty($password_confirm)) {
                if ($password === $password_confirm) {
                    $user = User::whereEmail($email)->firstOrFail();
                    $user->password = Hash::make($password);
                    $user->update();
                    Alert::success('FÉLICITATION! VOTRE MOT DE PASSE A ÉTÉ MODIFIIÉ AVEC SUCCÈS.')->persistent('Fermer'); // if well redirect page home
                    return redirect()->back()->with('success', 'FÉLICITATION! VOTRE MOT DE PASSE A ÉTÉ MODIFIIÉ AVEC SUCCÈS.');
                } else { // else stay same page to fix error form
                    Alert::error('Mot de passe ne correspond pas')->persistent('Fermer');
                    return redirect()->back()->with('error', 'Mot de passe non identique'); // rest on same page
                }
            }
        }

        $data = array(
            'title' => 'GS EMaquis - Changer mot de passe',
            'active_menu' => 'user',
            'active_sub_menu' => 'change_password',
            'main_content' => 'change_password',
            'id' => $user->id,
            'user' => $user,
        );
        return view('auth.passwords.change', $data); //pass data to vue load
    }

    /**
     * Update a user instance after a valid profil form.
     *
     * @param array $data
     * @return \App\User
     */
    protected function profile(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'username' => ['required', 'string', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'phone' => ['required', 'string', 'max:255', 'unique:users'],
                'password' => ['string', 'min:8', 'confirmed'],
                'password_confirm' => 'same:password',
            ]);
            // set instance user
            $user = User::whereId($id)->first();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->updated_at = date('Y-m-d H:i:s');
            if ($request->password) {
                // dd('Password defined');
                $user->password = Hash::make($request->password);
            }
            // update user
            $user->save();
            print_r('user updated');
            if (isset($user)) {
                $request->session()->put('user', $user);
                Alert::success('Votre profil a été modifié avec succès')->persistent('Fermer');
            } else {
                Alert::error('Une erreur est survenue pendant la modification de votre profil. Veuillez réssayer!!!')->persistent('Fermer');
            }
            return redirect()->back()->with('success', 'Votre profil a été modifié avec succès'); // rest on same page
        }
        // $user = Auth::user();
        $user = $request->session()->get('user');
        // dd($user);
        if ($user == null) {
            return redirect('login');
        }
        $data = array(
            'title' => 'GS EMaquis - Profil',
            'active_menu' => 'user',
            'active_sub_menu' => 'my_profile',
            'main_content' => 'my_profile',
            'id' => $user->id,
            'user' => $user,
        );
        return view('auth.profile', $data); //pass data to vue load

    }

    /**
     * lock screen
     *
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function lock(Request $request, $id = null)
    {
        $user = Auth::user();
        if ($request->isMethod('GET')) {
            $data = array(
                'title' => 'Mairie de San-Pedro | Site Officiel | Vérouiller',
                'active_menu' => 'user',
                '$active_sub_menu' => 'lock',
                'main_content' => 'lock',
                'id' => $user->id,
                'user' => $user,
            );
            return view('auth.lock', $data); //pass data to vue load
        } else {
            // get user
            $user = User::whereEmail($request->get('email'))->first();
            $data = array(
                'email' => $request['email'],
                'password' => $request['password'],
            );
            if (!$user) {
                Alert::error("Email incorrect !")->persistent('Fermer');
                return redirect()->back();
            } else {
                if ((Auth::attempt($data))) {
                    $request->session()->put('user', $user); // put user in session
                    $profile = Profile::whereId($user->profile_id)->first();
                    $request->session()->put('profile', $profile); // put user in session
                    return Redirect::to('/');
                } else {
                    Alert::error("Mot de passe incorrect")->persistent('Fermer');
                    return redirect()->back();
                }
            }
        }
    }
}
