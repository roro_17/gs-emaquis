<?php

namespace App\Http\Controllers;

use App\Category;
use App\Food;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Auth;
use Session;
use Redirect;
use DB;

class FoodController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    protected $dir = 'dist/img/foods';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all foods
        $foods = Food::orderBy('id', 'desc')->get();
//         $user = Auth::user(); // get user logged
        $user = $request->session()->get('user');
        // dd($user);
        /* if($user == null){
             return redirect('login');
         }*/
        // get all categories
        $categories = Category::where('active', 1)->orderBy('id', 'desc')->get();
        $data = array(
            'title' => '',
            'active_menu' => 'foods',
            'active_sub_menu' => 'foods',
            'main_content' => 'foods',
            'active_menu_parent' => 'configuration',
            'categories' => $categories,
            'id' => $user->id,
            'user' => $user,
            'foods' => $foods,
            'add' => true,
        );
        // print_r($foods); exit;
        return view('pages.configuration.foods', $data); //pass data to vue load
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r('OK'); exit;
        $id = $request->input('id');
        $food = Food::whereId($id)->first();
        if ($id) { // update food
            print_r('id defined');
            $image = '';
            // upload image
            if ($request->hasFile('image')) { // check if image uploaded
                $image = $request->file('image'); // get image uploaded
                if (empty($id) || $id == null) {
                    $new_name = $id . '.' . $image->getClientOriginalExtension(); //renam image with id plat
                } else {
                    $new_name = $id . '.' . $image->getClientOriginalExtension(); //renam image with id plat
                }
                $image->move(public_path($this->dir), $new_name); // $image->getClientOriginalName() upload image in folder public/assets/global/images/plats with renam.
                $image = $this->dir . '/' . $new_name;
            }

            if ($image != null && $image != "") {
                // dd('not upload image');
                $food->image = $image;
            }

            // dd($page);
            // set instance food
            $food = Food::whereId($id)->first();
            $food->code = $request->code;
            $food->name = $request->name;
            $food->category_id = $request->category;
            $food->unit_price = $request->unit_price;
            $food->quantity_stock = $request->quantity_stock;
            $food->active = 1;
            $food->updated_at = date('Y-m-d H:i:s');
            // update food
            $food->save();
            print_r('food updated');

            if ($food) {
                Alert::success("Plat modifié avec succès.")->persistent('Fermer');
                return redirect('foods')->with('success', 'Plat modifié avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('foods')->with('error', 'Erreur survenue lors de la la mise à jour. Veuillez réessayer!');
            }


        } else { // create food
            // get next id
            $statement = DB::select("show table status like 'foods'"); // requete pour recuperer le prochain id d'un autoincrement
            $id = $statement[0]->Auto_increment; // prochain id
            $image = '';
            // upload image
            // dd($request->hasFile('image'));
            if ($request->hasFile('image')) { // check if image uploaded
                $image = $request->file('image'); // get image uploaded
                // dd($image);
                if (empty($id) || $id == null) {
                    $new_name = $id . '.' . $image->getClientOriginalExtension(); //renam image with id plat
                } else {
                    $new_name = $id . '.' . $image->getClientOriginalExtension(); //renam image with id plat
                }
                $image->move(public_path($this->dir), $new_name); // $image->getClientOriginalName() upload image in folder public/assets/global/images/plats with renam.
                $image = $this->dir . '/' . $new_name;
                // dd($image);
            }


            // dd($page);
            $food = Food::create([
                'code' => $request->input('code'),
                'name' => $request->input('name'),
                'category_id' => $request->input('category'),
                'unit_price' => $request->unit_price,
                'quantity_stock' => $request->quantity_stock,
                'image' => $image,
                'active' => 1,
                'description' => "",
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if ($food) {
                Alert::success("Plat créé avec succès.")->persistent('Fermer');
                return redirect('foods')->with('success', 'Platcréé avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('foods')->with('error', 'Erreur survenue lors de l\'enregistrement. Veuillez réessayer!');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $delete = Food::where('id', $id)->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Food supprimé avec succès";
        } else {
            $success = true;
            $message = "Food non trouvée";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
