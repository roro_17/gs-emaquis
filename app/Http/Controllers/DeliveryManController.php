<?php

namespace App\Http\Controllers;

use App\DeliveryMan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Auth;
use Session;
use Redirect;
use DB;

class DeliveryManController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    protected $dir = 'dist/img/deliveries_mans';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all deliveryMans
        $deliveries_mans = DeliveryMan::orderBy('id', 'desc')->get();
        // $user = Auth::user(); // get user logged
        $user = $request->session()->get('user');
        // dd($user);
        /* if($user == null){
             return redirect('login');
         }*/
        $data = array(
            'title' => '',
            'active_menu' => 'deliveries_mans',
            'active_sub_menu' => 'deliveries_mans',
            'active_menu_parent' => 'configuration',
            'main_content' => 'deliveries_mans',
            'id' => $user->id,
            'user' => $user,
            'deliveries_mans' => $deliveries_mans,
            'add' => true,
        );
        // print_r($deliveryMans); exit;
        return view('pages.configuration.deliveries_mans', $data); //pass data to vue load
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r('OK'); exit;
        $id = $request->input('id');
        $deliveryMan = DeliveryMan::whereId($id)->first();
        if ($id) { // update deliveryMan
            print_r('id defined');

            // set instance deliveryMan
            $deliveryMan->name = $request->name;
            $deliveryMan->phone = $request->phone;
            $deliveryMan->email = $request->email;
            $deliveryMan->adress = $request->adress;
            $deliveryMan->updated_at = date('Y-m-d H:i:s');
            // update deliveryMan
            $deliveryMan->save();
            print_r('deliveryMan updated successful');

            if ($deliveryMan) {
                Alert::success("Livreur modifié avec succès.")->persistent('Fermer');
                return redirect('deliveries_mans')->with('success', 'Livreur modifié avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('deliveries_mans')->with('error', 'Erreur survenue lors de la la mise à jour. Veuillez réessayer!');
            }

        } else { // create deliveryMan
            // get next id
            $statement = DB::select("show table status like 'delivery_mans'"); // requete pour recuperer le prochain id d'un autoincrement
            $id = $statement[0]->Auto_increment; // prochain id

            $deliveryMan = DeliveryMan::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'adress' => $request->adress,
                'active' => 1,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);
            $deliveryMan->save();

            if ($deliveryMan) {
                Alert::success("Livreur créé avec succès.")->persistent('Fermer');
                return redirect('deliveries_mans')->with('success', 'Livreurcréé avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('deliveries_mans')->with('error', 'Erreur survenue lors de l\'enregistrement. Veuillez réessayer!');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $delete = DeliveryMan::where('id', $id)->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Livreur supprimé avec succès";
        } else {
            $success = true;
            $message = "Livreure non trouvé";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
