<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth'); // redirect page if user not logged
    }

    /**
     * Show the application live to pedro.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        // $user = Auth::user();
        $user = $request->session()->get('user');
        // dd($user);
       /* if($user == null){
            return redirect('login');
        }*/

        if ($user != null && $user->is_admin == 0){// profile users
            $orders = DB::table('orders')
                ->select(DB::raw('COUNT(*) AS order_count, SUM(`total_price`) AS order_mount, payment_mode, user_id'))
                ->where('user_id', '=', $user->id)
                ->groupBy('payment_mode', 'user_id')
                ->get();
        } else {// profile super admin or admin
            $orders = DB::table('orders')
                ->select(DB::raw('COUNT(*) AS order_count, SUM(`total_price`) AS order_mount, payment_mode, user_id'))
                ->groupBy('payment_mode', 'user_id')
                ->get();
        }

        // dd($orders);
        // load page home
        $data = array(
            'title' => 'GS EMaquis - Tableau de bord',
            'active_menu' => 'home',
            'active_menu_parent' => 'home',
            'main_content' => 'home',
			'user' => $user,
            'orders' => $orders
        );
        return view('home', $data); //pass data to vue load
    }
}
