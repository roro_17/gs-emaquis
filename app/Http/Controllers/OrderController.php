<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exports\TransactionsExport;
use App\Order;
use App\Food;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Alert;
use Excel;
use Auth;
use Session;
use Redirect;

class OrderController extends Controller
{
    //

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    // protected $dir = 'assets/global/images/orders';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $user = Auth::user();
        $user = $request->session()->get('user');
        // dd($user);
        if ($user == null) {
            return redirect('login');
        }
        // get all orders
        // $orders = Order::orderBy('id', 'desc')->get();
        if ($user->is_admin == 0){// profile users
            $orders = Order::where('user_id', $user->id)
                ->orderBy('id', 'DESC')
                ->get();
        } else {// profile super admin or admin
            $orders = Order::orderBy('id', 'desc')->get();
        }

        $data = array(
            'title' => 'GS EMaquis - Liste des opérations',
            'active_menu' => 'orders',
            'active_menu_parent' => 'orders',
            'orders' => $orders
        );
        // print_r($sliders); exit;
        return view('pages.orders', $data); //pass data to vue load
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // load form order fo creating record
        // $user = Auth::user(); // get user logged
        $user = $request->session()->get('user');
        // dd($user);
        if ($user == null) {
            return redirect('login');
        }
        // get all categories
        $categories = Category::orderBy('id', 'desc')
            ->where('active',"=",1)
            ->get();
        // dd($categories);
        // get all foods
        $foods = Food::orderBy('id', 'desc')
            ->where('category_id',"=" ,$categories[0]->id)
            ->get();
        // dd($foods);
        if ($request->isMethod('GET')) {
            $data = array(
                'title' => 'GS EMaquis - Nouvelle commande',
                'active_menu' => 'order',
                'active_menu_parent' => 'order',
                'main_content' => 'order',
                'categories' => $categories,
                'foods' => $foods,
                'category_id' => $categories[0]['id'],
            );
            return view('pages.order', $data); //pass data to vue load
        } else {
            // dd('test ok');




            // Redirect to index
            return redirect()->action('OrderController@create');
        }
    }

    /**
     * Get Order Number or Order Number
     */
    public function getOrderNumber()
    {
        $data_time = date('YmdHis') . substr(microtime(), 2, 6); // datetime
        $secret_key = 'f@b10d€$1g2OIO'; // secret key
        $order_number = strtoupper(hash_hmac('ripemd160', $data_time, $secret_key)); // hash order or number order
        return $order_number;
    }

    /**
     * Get Foods by Category
     *
     * @param Request $request
     * @param $category_id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getFoodsByCategory(Request $request, $category_id)
    {
        // load form order fo creating record
        // $user = Auth::user(); // get user logged
        $user = $request->session()->get('user');
        // dd($user);
        if ($user == null) {
            return redirect('login');
        }
        // get all categories
        $categories = Category::orderBy('id', 'desc')
            ->where('active',"=",1)
            ->get();
        // dd($categories);
        // get all foods
        $foods = Food::orderBy('id', 'desc')
            ->where('category_id',"=" ,$category_id)
            ->get();
        // dd($foods);
        if ($request->isMethod('GET')) {
            $data = array(
                'title' => 'GS EMaquis - Nouvelle commande',
                'active_menu' => 'order',
                'active_menu_parent' => 'order',
                'main_content' => 'order',
                'categories' => $categories,
                'foods' => $foods,
                'category_id' => $category_id,
            );
            return response()->json($data); // return data in son
        }
    }


}
