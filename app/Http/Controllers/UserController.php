<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticateUsers;
use App\Profile;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Session;
use Redirect;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all users
        $users = User::orderBy('id', 'desc')->get();
        $profiles = Profile::all();
        // $user = Auth::user();
        $user = $request->session()->get('user');
        // dd($user);
        if ($user == null) {
            return redirect('login');
        }
        $data = array(
            'title' => 'GS EMaquis - Utilisateurs',
            'active_menu' => 'users',
            'active_sub_menu' => 'users',
            'active_menu_parent' => 'users',
            'main_content' => 'users',
            'id' => $user->id,
            'user' => $user,
            'users' => $users,
            'profiles' => $profiles,
            'add' => true,
        );
        // print_r($users); exit;
        return view('pages.users', $data); //pass data to vue load
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r('OK'); exit;
        $id= $request->input('id');
        if($id){ // update user
            print_r('id defined');
            // set instance user
            $user=User::whereId($id)->first();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->profile_id = $request->profile;
            // update user
            // $user->attachRole($request->input('profile'));
            $user->save();
            print_r('user updated');

            if($user){
                Alert::success("Utilisateur modifié avec succès.")->persistent('Fermer');
                return redirect('users')->with('success', 'Utilisateur modifié avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('users')->with('error', 'Erreur survenue lors de la la mise à jour. Veuillez réessayer!');
            }
        } else { // create user
            // print_r($request->input('profile'));exit;
            $user = User::create([
                'name'       => $request->input('name'),
                'username'        => $request->input('username'),
                'email'            => $request->input('email'),
                'password'         => Hash::make($request->input('password')),
                'terms'            => $request->input('terms') ? 1:0,
                'profile_id'       => $request->input('profile'),
                'activated'        => 1,
            ]);

            // $user->attachProfile($request->input('profile'));
            $user->save();

            if($user){
                Alert::success("Utilisateur créé avec succès.")->persistent('Fermer');
                return redirect('users')->with('success', 'Utilisateur créé avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('users')->with('error', 'Erreur survenue lors de l\'enregistrement. Veuillez réessayer!');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $delete = User::where('id', $id)->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Utilisateur supprimé avec succès";
        } else {
            $success = true;
            $message = "Utilisateur non trouvé";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
