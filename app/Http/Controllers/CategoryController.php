<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Alert;
use Excel;
use Auth;
use Session;
use Redirect;
use DB;
use SweetAlert;
// use RealRashid\SweetAlert\Facades\Alert;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    protected $dir = 'dist/img/categories';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // get all categorys
        $categories = Category::orderBy('id', 'desc')->get();
        // $user = Auth::user(); // get user logged
        $user = $request->session()->get('user');
        // dd($user);
        if($user == null){
             return redirect('login');
         }

        $data = array(
            'title' => '',
            'active_menu' => 'categories',
            'active_sub_menu' => 'categories',
            'active_menu_parent' => 'configuration',
            'main_content' => 'categories',
            'id' => $user->id,
            'user' => $user,
            'categories' => $categories,
            'add' => true,
        );
        // print_r($categorys); exit;
        return view('pages.configuration.categories', $data); //pass data to vue load
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // print_r('OK'); exit;
        $id = $request->input('id');
        $category = Category::whereId($id)->first();
        if ($id) { // update category
            print_r('id defined');

            // set instance category
            $category->code = $request->code;
            $category->name = $request->name;
            $category->updated_at = date('Y-m-d H:i:s');
            // update category
            $category->save();
            print_r('category updated successful');

            if ($category) {
                Alert::success("Catégorie modifié avec succès.")->persistent('Fermer');
                return redirect('categorys')->with('success', 'Catégorie modifié avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('categorys')->with('error', 'Erreur survenue lors de la la mise à jour. Veuillez réessayer!');
            }

        } else { // create category
            // get next id
           // dd('create ok');
            $statement = DB::select("show table status like 'categories'"); // requete pour recuperer le prochain id d'un autoincrement
            $id = $statement[0]->Auto_increment; // prochain id

            $category = Category::create([
                'code' => $request->input('code'),
                'name' => $request->input('name'),
                'active' => 1,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s'),
            ]);

            if ($category) {
                Alert::success("Catégorie créé avec succès.")->persistent('Fermer');
                return redirect('categories')->with('success', 'Catégorie créé avec succès.');
            } else {
                Alert::error("Erreur survenue lors de l'enregistrement. Veuillez réessayer!")->persistent('Fermer');
                return redirect('categories')->with('error', 'Erreur survenue lors de l\'enregistrement. Veuillez réessayer!');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $delete = Category::where('id', $id)->delete();

        // check data deleted or not
        if ($delete == 1) {
            $success = true;
            $message = "Catégorie supprimée avec succès";
        } else {
            $success = true;
            $message = "Catégoriee non trouvée";
        }

        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }
}
