<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Food extends Model
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'foods';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', // 'string'
        'image', // 'string'
        'id', // 'string',
        'active', // tint
        'name', //'string'
        'code', //'string'
        'unit_price', // integer
        'quantity_stock',
        'category_id', // category food
        'created_at',
        'updated_at',
    ];

    /**
     * A Food belongs to a Category.
     *
     * @return mixed
     */
    public function Category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Get Category of Food.
     *
     * @return mixed
     */
    public static function getCategory($id)
    {
        return Category::findOrFail($id)->name;
    }

    public function attachCategory($category)
    {
        return $this->category()->attach($category);
    }
}
