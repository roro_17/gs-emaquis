<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Fillable fields for a Profile.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    protected $casts = [
        'level' => 'integer',
    ];


    /**
     * Obtenir un extrait de string
     *
     * @param String $str Chaîne pour obtenir un extrait de
     * @param Integer $startPos Position int string pour commencer à extraire
     * @param Integer $maxLength La longueur maximale de la chaîne à extraire
     * @return Chaîne extraite
     */
    public static function getExcerpt($str, $startPos = 0, $maxLength = 50) {
        if(strlen($str) > $maxLength) {
            $excerpt   = substr($str, $startPos, $maxLength - 6);
            $lastSpace = strrpos($excerpt, ' ');
            $excerpt   = substr($excerpt, 0, $lastSpace);
            $excerpt  .= ' [...]';
        } else {
            $excerpt = $str;
        }

        return $excerpt;
    }

    /**
     * Profile User Relationships.
     *
     * @var array
     */
    public function user()
    {
        return $this->hasOne('App\User');
    }

    // User User Setup - SHould move these to a trait or interface...

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function hasUser($name)
    {
        foreach ($this->users as $user) {
            if ($user->name == $name) {
                return true;
            }
        }

        return false;
    }

    public function assignUser($user)
    {
        return $this->users()->attach($user);
    }

    public function removeUser($user)
    {
        return $this->users()->detach($user);
    }
}
