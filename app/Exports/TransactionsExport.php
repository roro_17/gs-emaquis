<?php

namespace App\Exports;

use App\Transaction;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithTitle;

class TransactionsExport implements FromArray, ShouldAutoSize, WithHeadings,WithTitle//, WithDrawings
{
    protected $transactions;
    protected $headings = [
        '#',
        'ID Transaction',
        'Téléphone',
        'Montant',
        'Date',
        'Statut'
    ];

    public function __construct(array $transactions)
    {
        $this->transactions = $transactions;
    }

    public function array(): array
    {
        return $this->transactions;
    }

    public function headings(): array
    {
        return $this->headings;
    }

   /* public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo');
        $drawing->setDescription('Liste des opérations');
        $drawing->setPath(public_path('/dist/img/logo_white.png'));
        $drawing->setHeight(90);
        $drawing->setCoordinates('B1');

        return $drawing;
    }*/

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Liste des transactions';
    }
}
