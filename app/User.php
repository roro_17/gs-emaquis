<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'phone',
        'password',
        'active',
        'is_admin',
        'profile_id',
        'terms',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * A Request belongs to a profile.
     *
     * @return mixed
     */
    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    /**
     * Get profile of Request.
     *
     * @return mixed
     */
    public static function getProfile($id)
    {
        return Profile::findOrFail($id)->name;
    }

    public function attachProfile($profile)
    {
        return $this->profile()->attach($profile);
    }

    // Request Request Setup - SHould move these to a trait or interface...

    public function transaction()
    {
        return $this->belongsToMany('App\Transaction')->withTimestamps();
    }

    public function hasUser($name)
    {
        foreach ($this->requests as $request) {
            if ($request->name == $name) {
                return true;
            }
        }

        return false;
    }

    public function assignTransaction($transaction)
    {
        return $this->transaction()->attach($transaction);
    }

    public function removeTransaction($transaction)
    {
        return $this->transaction()->detach($transaction);
    }

    /**
     * Get name user by id.
     *
     * @return mixed
     */
    public static function getNameUserById($id)
    {
        return User::findOrFail($id)->name;
    }
}
