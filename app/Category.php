<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Category extends Model
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'code',
        'name',
        'active',
        'created_at',
        'updated_at',
    ];

    /**
     * Categorie Food Relationships.
     *
     * @var array
     */
    public function food()
    {
        return $this->hasOne('App\Food');
    }

    // Food Food Setup - SHould move these to a trait or interface...

    public function foods()
    {
        return $this->belongsToMany('App\Food')->withTimestamps();
    }

    public function hasFood($name)
    {
        foreach ($this->foods as $food) {
            if ($food->name == $name) {
                return true;
            }
        }

        return false;
    }

    public function assignFood($food)
    {
        return $this->foods()->attach($food);
    }

    public function removeFood($food)
    {
        return $this->foods()->detach($food);
    }
}
