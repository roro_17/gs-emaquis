<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'reference',
        'total_price',
        'description',
        'delivery_adresse',
        'number_foods',
        'payment_mode',
        'status', // annuler livrée, en cours de préparation
        'created_at',
        'updated_at',
    ];


    /**
     * A User belongs to a user.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get user of User.
     *
     * @return mixed
     */
    public static function getUser($id)
    {
        return User::findOrFail($id)->name;
    }

    public function attachUser($user)
    {
        return $this->user()->attach($user);
    }

    public static function insertData($data){
        DB::table('transactions')->insert($data);
    }

}
