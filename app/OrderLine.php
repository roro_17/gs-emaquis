<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class OrderLine extends Model
{
    //

    protected $table = 'orders_lines';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'order_id',
        'food_id',
        'quantity',
        'total_price',
        'created_at',
        'updated_at',
    ];

    /**
     * A OrderLine belongs to a order.
     *
     * @return mixed
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * A OrderLine belongs to a food.
     *
     * @return mixed
     */
    public function food()
    {
        return $this->belongsTo('App\Food');
    }

}
